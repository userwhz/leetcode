package com.whzcloud.leetCode19;

public class LeetCode1930 {
    public static void main(String[] args) {
        System.out.println(new Solution1930().numWays(7));
    }
}

class Solution1930 {
    public int numWays(int n) {
        if (n == 0 || n == 1){
            return 1;
        }
        int res = 1;
        int pre = 1;
        int cur = 1;
        for (int i = 1; i < n; i++){
            res = (pre + cur) % 1000000007;
            pre = cur;
            cur = res;
        }
        return res;
    }
}
