package com.whzcloud.leetCode19;

import java.util.Arrays;

public class LeetCode1922 {
    public static void main(String[] args) {
        Solution1922 solution = new Solution1922();
        int[] nums = {2, 3, 1, 0, 2, 5, 3};
        System.out.println(solution.findRepeatNumber(nums));
    }
}


class Solution1922 {
    public int findRepeatNumber(int[] nums) {
        Arrays.sort(nums);
        for (int i = 1; i < nums.length; i++){
            if (nums[i] == nums[i - 1]){
                return nums[i];
            }
        }
        return 0;
    }
}