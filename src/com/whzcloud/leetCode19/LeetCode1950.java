package com.whzcloud.leetCode19;

import java.util.Stack;

public class LeetCode1950 {
    public static void main(String[] args) {
        MinStack1950 minStack = new MinStack1950();

    }
}

class MinStack1950 {
    Stack<Integer> A, B;
    /** initialize your data structure here. */
    public MinStack1950() {
        A = new Stack<>();
        B = new Stack<>();
    }

    public void push(int x) {
        A.add(x);
        if(B.empty() || B.peek() >= x){
            B.add(x);
        }
    }

    public void pop() {
        if(A.pop().equals(B.peek())){
            B.pop();
        }
    }

    public int top() {
        return A.peek();
    }

    public int min() {
        return B.peek();
    }
}


