package com.whzcloud.leetCode19;

public class LeetCode1929 {
    public static void main(String[] args) {

    }
}

class Solution1929 {
    public int fib(int n) {
        if (n == 0){
            return 0;
        }else if (n == 1){
            return 1;
        }
        int res = 1;
        int pre = 0;
        int cur = 1;
        for (int i = 1; i < n; i++){
            res = (pre + cur) % 1000000007;
            pre = cur;
            cur = res;
        }
        return res;
    }
}