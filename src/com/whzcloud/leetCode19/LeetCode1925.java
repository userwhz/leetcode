package com.whzcloud.leetCode19;

import java.util.Stack;

public class LeetCode1925 {
    public static void main(String[] args) {
        ListNode head = new ListNode(1);
        ListNode node1 = new ListNode(3);
        ListNode node2 = new ListNode(2);
        head.next = node1;
        node1.next = node2;
        int[] res = new Solution1925().reversePrint(head);
        for (int i = 0; i < res.length; i++){
            System.out.println(res[i]);
        }

    }
}

class Solution1925 {
    public int[] reversePrint(ListNode head) {
        int i = 0;
        Stack<Integer> stack = new Stack<>();
        ListNode cur = head;
        while (cur != null){
            stack.push(cur.val);
            cur = cur.next;
            i++;
        }
        int[] res = new int[i];
        for (int j = 0; j < i; j++){
            res[j] = stack.pop();
        }
        return res;

    }
}

class ListNode {
      int val;
      ListNode next;
      ListNode(int x) { val = x; }
}
