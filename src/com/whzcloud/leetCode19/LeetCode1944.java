package com.whzcloud.leetCode19;


public class LeetCode1944 {
    public static void main(String[] args) {

    }
}

class Solution1944 {
    public ListNode reverseList(ListNode head) {
        ListNode pre = null;
        ListNode cur = head;
        while (cur != null) {
            ListNode next = cur.next;
            cur.next = pre;
            pre = cur;
            cur = next;
        }
        return pre;
    }
}

