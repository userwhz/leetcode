package com.whzcloud.leetCode19;

import java.util.Stack;

public class LeetCode1984 {
    public static void main(String[] args) {
        System.out.println(new Solution1984().reverseWords("the sky is blue"));
    }
}
class Solution1984 {
    public String reverseWords(String s) {
        s = s.trim(); // 删除首尾空格
        Stack<String> stack = new Stack<>();
        int count = 0;
        for (int i = 0; i < s.length(); i++){
            char c = s.charAt(i);
            if (c == ' ' ){
                if (i > 0 &&s.charAt(i - 1) == ' '){
                    continue;
                }
                stack.push(s.substring(i - count, i));
                count = 0;
            }else{
                if (i == s.length() - 1){
                    stack.push(s.substring(i - count, i + 1));
                }
                count++;
            }
        }
        StringBuffer sb = new StringBuffer();
        while (!stack.isEmpty()){
            sb.append(stack.pop());
            sb.append(" ");
        }
        return sb.toString().trim();
    }
}
