package com.whzcloud.leetCode19;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class LeetCode1961 {
    public static void main(String[] args) {
        int[] nums = {1, 2, 3, 2, 2, 2, 5, 4, 2};
        System.out.println(new Solution1961().majorityElement(nums));
    }
}


class Solution1961 {
    public int majorityElement(int[] nums) {
        Map<Integer,Integer> ant = new HashMap<>();
        for(int num : nums){
            ant.put(num, ant.getOrDefault(num, 0) + 1);
            if(ant.get(num) > nums.length / 2) return num;
        }
        return 0;
    }
}

