package com.whzcloud.leetCode19;

import com.sun.media.sound.AiffFileReader;

public class LeetCode1924 {
    public static void main(String[] args) {
        Solution1924 solution = new Solution1924();
        System.out.println(solution.replaceSpace("We are happy."));
    }
}
class Solution1924 {
    public String replaceSpace(String s) {
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < s.length(); i++){
            if (s.charAt(i) == ' '){
                sb.append("%20");
            }else {
                sb.append(s.charAt(i));
            }
        }
        return sb.toString();
    }
}