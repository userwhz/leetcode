package com.whzcloud.leetCode19;

public class LeetCode1923 {
    public static void main(String[] args) {
        Solution1923 solution = new Solution1923();
        int[][] matrix = {{1,   4,  7, 11, 15}
                        ,{2,   5,  8, 12, 19}
                        ,{3,   6,  9, 16, 22}
                        ,{10, 13, 14, 17, 24}
                        ,{18, 21, 23, 26, 30}};
        System.out.println(solution.findNumberIn2DArray(matrix, 1));
    }
}

class Solution1923 {
    public boolean findNumberIn2DArray(int[][] matrix, int target) {
        for (int i = 0; i < matrix.length; i++){
            for (int j = 0; j < matrix[0].length; j++){
                if (matrix[i][j] == target){
                    return true;
                }
            }
        }
        return false;
    }
}
