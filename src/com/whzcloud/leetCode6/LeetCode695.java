package com.whzcloud.leetCode6;

public class LeetCode695 {

}

class Solution695 {
    int[] dx = {0, 0, 1, -1};
    int[] dy = {1, -1, 0, 0};
    public int maxAreaOfIsland(int[][] grid) {
        int ans = 0;
        for (int i = 0; i < grid.length; i++) {
            for (int j = 0; j < grid[0].length; j++) {
                if (grid[i][j] == 0) {
                    continue;
                }
                ans = Math.max(ans, dfs(grid, i , j));
            }
        }
        return ans;
    }

    private int dfs(int[][] grid, int i, int j) {
        if (i < 0 || i > grid.length - 1 || j < 0 || j > grid[0].length - 1 ||
        grid[i][j] != 1) {
            return 0;
        }
        grid[i][j] = 0;
        int ans = 1;
        for (int k = 0; k < 4; k++) {
            int next_i = i + dx[k];
            int next_j = j + dy[k];
            ans += dfs(grid, next_i, next_j);
        }
        return ans;
    }
}