package com.whzcloud.leetCode6;

public class LeetCode680 {
    public static void main(String[] args) {
        System.out.println(new Solution680().validPalindrome("aba"));
    }
}

class Solution680 {
    public boolean validPalindrome(String s) {
        int left = 0;
        int right = s.length() - 1;
        int cnt = 0;
        while (left < right) {
            if (s.charAt(left) != s.charAt(right)) {
                return validPalindromeHelper(s, left + 1, right) || validPalindromeHelper(s, left, right - 1);
            }
            left++;
            right--;
        }
        return true;
    }

    private boolean validPalindromeHelper(String s, int left, int right) {
        while (left < right) {
            if (s.charAt(left) != s.charAt(right)) {
                return false;
            }
            left++;
            right--;
        }
        return true;
    }
}
