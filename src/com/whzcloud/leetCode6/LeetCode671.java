package com.whzcloud.leetCode6;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.TreeSet;

public class LeetCode671 {
    public static void main(String[] args) {

    }
}

class Solution671 {
    List<Integer> list = new ArrayList<>();
    public int findSecondMinimumValue(TreeNode root) {
        dfs(root);
        Collections.sort(list);
        int temp = list.get(0);
        if(temp  == list.get(list.size() - 1)){
            return -1;
        }
        for(int i = 0; i < list.size();i++){
            if(list.get(i) != temp){
                return list.get(i);
            }
        }
        return 0;
    }

    public void dfs(TreeNode root){
        if(root != null){
            list.add(root.val);
            dfs(root.left);
            dfs(root.right);
        }
    }
}

class TreeNode {
    int val;
    TreeNode left;
    TreeNode right;
    TreeNode() {}
    TreeNode(int val) { this.val = val; }
    TreeNode(int val, TreeNode left, TreeNode right) {
    this.val = val;
    this.left = left;
    this.right = right;
}
}
