package com.whzcloud.offer1;

public class Offer22 {
}

class SolutionO22 {
    public ListNode getKthFromEnd(ListNode head, int k) {
       ListNode slow = head;
       ListNode fast = head;
       while (k > 0) {
           k--;
           fast = fast.next;
       }
        while (fast != null){
            slow = slow.next;
            fast = fast.next;
        }
        return slow;
    }
}