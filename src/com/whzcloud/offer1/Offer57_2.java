package com.whzcloud.offer1;

import java.util.ArrayList;
import java.util.List;

public class Offer57_2 {
}

class SolutionO57_2 {
    public int[][] findContinuousSequence(int target) {
        int left = 1;
        int right = 1;
        int sum = 1;

        List<int[]> res = new ArrayList<>();

        while (left <= target / 2) {
            if (sum < target) {
                right++;
                sum += right;
            } else if (sum > target) {
                sum -= left;
                left++;
            }else {
                int[] arr = new int[right - left + 1];
                for (int i = left; i <= right; i++) {
                    arr[i - left] = i;
                }
                res.add(arr);
                sum -= left;
                left++;
            }
        }

        return res.toArray(new int[res.size()][]);
    }
}
