package com.whzcloud.offer1;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class Offer56_1 {
}

class SolutionO56_1 {
    public int[] singleNumbers(int[] nums) {
        int ret = 0;
        for (int num: nums) {
            ret ^= num;
        }
        int target = 1;
        while ((target & ret) == 0) {
            target = target << 1;
        }
        int a = 0;
        int b = 0;
        for (int num: nums) {
            if ((num & target) == 0) {
                a ^= num;
            }else {
                b ^= num;
            }
        }
        return new int[]{a, b};
    }
}