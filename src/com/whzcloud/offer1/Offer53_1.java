package com.whzcloud.offer1;

public class Offer53_1 {
}

class SolutionO53_1 {
    public int search(int[] nums, int target) {
        int len = 0;
        for (int i = 0; i < nums.length; i++) {
            if (nums[i] == target) {
                len++;
            }
            if (nums[i] > target) {
                break;
            }
        }
        return len;
    }
}