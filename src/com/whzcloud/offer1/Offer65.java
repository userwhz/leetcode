package com.whzcloud.offer1;

public class Offer65 {

}

class SolutionO65 {
    public int add(int a, int b) {
        while (b != 0) {
            int c = (a & b) << 1;
            a ^= b;
            b = c;
        }
        return a;
    }
}