package com.whzcloud.offer1;

public class Offer54 {
}


class SolutionO54 {
    int res;
    int k;
    public int kthLargest(TreeNode root, int k) {
        this.k = k;
        dfs(root);
        return res;
    }

    private void dfs(TreeNode root) {
        if (root == null) {
            return;
        }
        dfs(root.right);
        if (k == 0) {
            return;
        }
        k--;
        if (k == 0) {
            res = root.val;
        }
        dfs(root.left);
    }
}