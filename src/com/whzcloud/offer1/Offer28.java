package com.whzcloud.offer1;

public class Offer28 {
    public static void main(String[] args) {
        ThreadLocal<Integer> first = new ThreadLocal<>();
        ThreadLocal<Integer> second = new ThreadLocal<>();


        System.out.println(first.get());
        System.out.println(second.get());
        second.set(12);
        new Thread(()->{
            second.set(1);
        }).start();

    }
}




class SolutionO28 {
    public boolean isSymmetric(TreeNode root) {
        return root == null ? true : recur(root.left, root.right);
    }

    private boolean recur(TreeNode L, TreeNode R) {
        if (L == null && R == null) {
            return true;
        }
        if (L == null || R == null || L.val != R.val) {
            return false;
        }
        return recur(L.left, R.right) && recur(L.right, R.left);
    }
}