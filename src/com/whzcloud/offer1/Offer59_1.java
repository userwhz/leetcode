package com.whzcloud.offer1;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.LinkedList;

public class Offer59_1 {
}

class SolutionO59_1 {
    public int[] maxSlidingWindow(int[] nums, int k) {
        if(nums.length == 0 || k == 0) {
            return new int[0];
        }
        Deque<Integer> deque = new ArrayDeque<>();
        int n = nums.length;
        int[] res = new int[n - k + 1];
        for (int i = 0, j = 0; i < n; i++) {
            while (!deque.isEmpty() && i - k + 1 > deque.getFirst()) {
                deque.pollFirst();
            }
            while (!deque.isEmpty() && nums[i] > nums[deque.getLast()]) {
                deque.pollLast();
            }
            deque.offer(i);
            if (i - k + 1 >= 0) {
                res[j] = nums[deque.getFirst()];
                j++;
            }
        }
        return res;
    }
}