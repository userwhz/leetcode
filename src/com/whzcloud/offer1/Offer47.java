package com.whzcloud.offer1;

public class Offer47 {
}

class SolutionO47 {
    public int maxValue(int[][] grid) {
        int r = grid.length;
        int c = grid[0].length;
        int[][] dp = new int[r + 1][c + 1];

        for (int i = 0; i < r + 1; i++) {
            dp[i][0] = 0;
        }
        for (int i = 0; i < c + 1; i++) {
            dp[0][i] = 0;
        }
        for (int i = 1; i < r + 1; i++) {
            for (int j = 1; j < c + 1; j++) {
                dp[i][j] = Math.max(dp[i - 1][j], dp[i][j - 1]) + grid[i - 1][j - 1];
            }
        }
        return dp[r][c];
    }
}