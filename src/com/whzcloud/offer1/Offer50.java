package com.whzcloud.offer1;

import java.util.HashMap;

public class Offer50 {

}

class SolutionO50 {
    public char firstUniqChar(String s) {
        HashMap<Character, Boolean> dic = new HashMap<>();
        char[] sc = s.toCharArray();
        for (char c : sc) {
            dic.put(c, !dic.containsKey(c));
        }
        for(char c : sc){
            if(dic.get(c)) {
                return c;
            }
        }
        return ' ';
    }
}