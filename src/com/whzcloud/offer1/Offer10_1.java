package com.whzcloud.offer1;

public class Offer10_1 {
    public static void main(String[] args) {
        System.out.println(new SolutionO10_1().fib(54));
    }
}

class SolutionO10_1 {
    public int fib(int n) {
        final int MOD = 1000000007;
        if (n == 0) {
            return 0;
        }else if (n == 1) {
            return 1;
        }
        int pre = 0;
        int cur = 1;
        int res = 0;
        for (int i = 2; i <= n; i++) {
            res = (pre + cur) % MOD;
            pre = cur;
            cur = res;
        }
        return res;
    }
}