package com.whzcloud.offer1;

public class Offer63 {
}


class SolutionO63 {
    public int maxProfit(int[] prices) {
        if (prices.length == 0) {
            return 0;
        }
        int res = 0;
        int min = prices[0];
        for (int i = 1; i < prices.length; i++) {
            min = Math.min(min, prices[i - 1]);
            res = Math.max(res, prices[i] - min);
        }
        return res;

    }
}