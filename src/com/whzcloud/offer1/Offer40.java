package com.whzcloud.offer1;

import java.util.Arrays;

public class Offer40 {
    public static void main(String[] args) {
        int[] arr = {3,2,1};
        int[] leastNumbers = new SolutionO40().getLeastNumbers(arr, 2);
        for (int s : leastNumbers) {
            System.out.println(s);
        }
    }
}


class SolutionO40 {
    public int[] getLeastNumbers(int[] arr, int k) {
        Arrays.sort(arr);
        int[] res = new int[k];
        for (int i = 0; i < k; i++) {
            res[i] = arr[i];
        }
        return res;
    }
}