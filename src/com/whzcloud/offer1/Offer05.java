package com.whzcloud.offer1;

public class Offer05 {
    public static void main(String[] args) {
        System.out.println(new SolutionO05().replaceSpace("asd sda"));
    }
}

class SolutionO05 {
    public String replaceSpace(String s) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) != ' '){
                sb.append(s.charAt(i));
            }else {
                sb.append("%20");
            }
        }
        return sb.toString();
    }
}