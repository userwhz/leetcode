package com.whzcloud.offer1;

public class Offer55_1 {
}



class SolutionO55_1 {
    public int maxDepth(TreeNode root) {
        if (root == null) {
            return 0;
        }
        int maxLeft = maxDepth(root.left) + 1;
        int maxRight = maxDepth(root.right) + 1;
        return Math.max(maxLeft, maxRight);
    }

}