package com.whzcloud.offer1;

public class Offer16 {
    public static void main(String[] args) {
        System.out.println(new SolutionO16().myPow(2.0, -2));
    }
}


class SolutionO16 {
    public double myPow(double x, int n) {
        long N = n;
        return N > 0 ? quickMul(x, N) : 1.0 / quickMul(x, N);
    }

    private double quickMul(double x, long N) {
        if (N == 0) {
            return 1;
        }
        double y = quickMul(x, N / 2);
        return N % 2 == 0? y * y : y * y * x;
    }
}