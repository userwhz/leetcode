package com.whzcloud.offer1;

public class Offer58_2 {
}

class SolutionO58_2 {
    public String reverseLeftWords(String s, int n) {
        int len = s.length();
        char[] chars = new char[len];
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < len - n; i++) {
            chars[i] = s.charAt(i + n);
            sb.append(chars[i]);
        }
        chars[n] = s.charAt(n);
        for (int i = len - n, j = 0; i < len; i++, j++) {
            chars[i] = s.charAt(j);
            sb.append(chars[i]);

        }
        return sb.toString();
    }
}