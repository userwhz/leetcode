package com.whzcloud.offer1;

import java.util.LinkedList;
import java.util.PriorityQueue;
import java.util.Queue;

public class Offer13 {
    public static void main(String[] args) {
        System.out.println(new SolutionO13().movingCount(38,15,9));
    }
}

class SolutionO13 {
    int[] dx = {0, -1, 1, 0};
    int[] dy = {-1, 0, 0, 1};
    public int movingCount(int m, int n, int k) {
        if (k == 0) {
            return 1;
        }
        boolean[][] visited = new boolean[m][n];
        visited[0][0] = true;
        int res = 1;
        Queue<int[]> queue = new LinkedList();
        queue.add(new int[]{0, 0});
        while (!queue.isEmpty()) {
            int len = queue.size();
            for (int i = 0; i < len; i++) {
                int[] temp = queue.poll();
                int x = temp[0];
                int y = temp[1];
                for (int j = 0; j < 4; j++) {
                    int tx = x + dx[j];
                    int ty = y + dy[j];
                    if (tx >= 0 && tx < m && ty >= 0 && ty < n && visited[tx][ty] == false) {
                        int sum = get(tx) + get(ty);
//                        int sum = 0;
//                        while (tx != 0) {
//                            sum += tx % 10;
//                            tx /= 10;
//                        }
//                        while (ty != 0) {
//                            sum += ty % 10;
//                            ty /= 10;
//                        }

                        if (sum <= k){
                            visited[tx][ty] = true;
                            res++;
                            queue.add(new int[]{tx, ty});
                        }
                    }
                }
            }
        }
        for (int i = 0; i < visited.length; i++) {
            for (int j = 0; j < visited[0].length; j++) {
                System.out.print(visited[i][j]);
            }
            System.out.println();
        }
        return res;
    }
    private int get(int x) {
        int res = 0;
        while (x != 0) {
            res += x % 10;
            x /= 10;
        }
        return res;
    }

}