package com.whzcloud.offer1;

import java.util.ArrayList;
import java.util.Arrays;

public class Offer39 {
}

class SolutionO39 {
    public int majorityElement(int[] nums) {
        Arrays.sort(nums);
        return nums[nums.length / 2];
    }
}