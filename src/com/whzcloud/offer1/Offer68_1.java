package com.whzcloud.offer1;

public class Offer68_1 {
}

class SolutionO68_1 {
    public TreeNode lowestCommonAncestor(TreeNode root, TreeNode p, TreeNode q) {
        while (root != null) {
            if (root.val < p.val && root.val < q.val) {
                root = root.right;
            }else if (root.val > p.val && root.val > q.val) {
                root = root.left;
            }else {
                break;
            }
        }
        return root;
    }
}