package com.whzcloud.offer1;

import java.util.Stack;

public class Offer31 {

}

class SolutionO31 {
    public boolean validateStackSequences(int[] pushed, int[] popped) {
        Stack<Integer> stack = new Stack<>();
        int cnt = 0;
        for (int i = 0; i < pushed.length; i++) {
            stack.push(pushed[i]);
            while (!stack.isEmpty() && stack.peek() == popped[cnt]) {
                stack.pop();
                cnt++;
            }
        }
        return stack.isEmpty();
    }
}