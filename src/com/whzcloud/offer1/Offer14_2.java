package com.whzcloud.offer1;

import java.math.BigInteger;
import java.util.Arrays;

public class Offer14_2 {

}


class SolutionO14_2 {
    public int cuttingRope(int n) {
        if (n <= 3) {
            return n - 1;
        }
        long res = 1;
        while(n > 4){
            res  = res * 3 % 1000000007;
            n -= 3;
        }
        return (int) (res * n % 1000000007);
    }
}