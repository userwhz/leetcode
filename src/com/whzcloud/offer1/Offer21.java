package com.whzcloud.offer1;

public class Offer21 {
}

class SolutionO21 {
    public int[] exchange(int[] nums) {
        int k = 0;
        for (int i = 0; i < nums.length; i++) {
            if(nums[i] % 2 != 0) {
                int temp = 0;
                temp = nums[i];
                nums[i] = nums[k];
                nums[k] = temp;
                k++;
            }
        }
        return nums;
    }
}