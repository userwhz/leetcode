package com.whzcloud.offer1;

public class Offer17 {
    public static void main(String[] args) {
        int[] ints = new SolutionO17().printNumbers(3);
        for (int i : ints) {
            System.out.println(i);
        }
    }
}

class SolutionO17 {
    public int[] printNumbers(int n) {
        int cnt = 1;
        for (int i = 0; i < n; i++) {
            cnt *= 10;
        }
        int[] res = new int[cnt - 1];
        for (int i = 1; i < cnt; i++) {
            res[i - 1] = i;
        }
        return res;
    }
}