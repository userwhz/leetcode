package com.whzcloud.offer1;

import java.util.*;

public class Offer32_3 {
    public static void main(String[] args) {
        Stack<Integer> stack = new Stack<>();
        stack.add(1);
        System.out.println(stack.size());
        TreeNode node15 = new TreeNode(15);
        TreeNode node7 = new TreeNode(7);
        TreeNode node9 = new TreeNode(9);
        TreeNode node20 = new TreeNode(20);
        TreeNode node3 = new TreeNode(3);
        node3.left = node9;
        node3.right = node20;
        node20.left = node15;
        node20.right = node7;

        List<List<Integer>> lists = new SolutionO32_3().levelOrder(node3);
        for (int i = 0; i < lists.size(); i++) {
            System.out.println(lists.get(i));
        }
    }
}


class SolutionO32_3 {
    public List<List<Integer>> levelOrder(TreeNode root) {
        if(root == null) {
            return new ArrayList<>();
        }
        List<List<Integer>> lists = new ArrayList<>();
        Queue<TreeNode> queue = new LinkedList<>();
        queue.add(root);
        int flag = 1;
        while (!queue.isEmpty()) {
            int len = queue.size();
            List<Integer> list = new ArrayList();
            Stack<Integer> stack = new Stack<>();
            for (int i = 0; i < len; i++) {
                if(flag == 1) {
                    TreeNode temp = queue.poll();
                    list.add(temp.val);
                    if (temp.left != null) {
                        queue.add(temp.left);
                    }
                    if (temp.right != null) {
                        queue.add(temp.right);
                    }
                } else {
                    TreeNode temp = queue.poll();
                    stack.push(temp.val);
                    if (temp.left != null) {
                        queue.add(temp.left);
                    }
                    if (temp.right != null) {
                        queue.add(temp.right);
                    }
                }
            }
            if (flag == -1){
                len = stack.size();
                for (int i = 0; i < len; i++) {
                    list.add(stack.pop());

                }
            }
            lists.add(list);
            flag = -flag;
        }
        return lists;
    }
}