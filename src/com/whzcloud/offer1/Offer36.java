package com.whzcloud.offer1;

public class Offer36 {
}

class SolutionO36 {
    Node pre;
    Node head;
    public Node treeToDoublyList(Node root) {
        if (root == null) {
            return null;
        }
        dfs(root);
        pre.right = head;
        head.left = pre;

        return head;
    }

    private void dfs(Node cur) {
        if (cur == null){
            return;
        }
        dfs(cur.left);

        if (pre != null) {
            pre.right = cur;
        }else {
            head = cur;
        }

        cur.left = pre;

        pre = cur;

        dfs(cur.right);
    }

}


class Node {
    public int val;
    public Node left;
    public Node right;

    public Node() {}

    public Node(int _val) {
        val = _val;
    }

    public Node(int _val, com.whzcloud.offer1.Node _left, com.whzcloud.offer1.Node _right) {
        val = _val;
        left = _left;
        right = _right;
    }
};