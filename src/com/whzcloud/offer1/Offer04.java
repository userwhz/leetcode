package com.whzcloud.offer1;

public class Offer04 {

}

class SolutionO04 {
    public boolean findNumberIn2DArray(int[][] matrix, int target) {
        if (matrix == null || matrix.length == 0 || matrix[0].length == 0) {
            return false;
        }
        int x = matrix.length - 1;
        int y = 0;
        int first = matrix[x][y];
        while (true) {
            if (first < target) {
                y += 1;
                if (y > matrix[0].length - 1) {
                    return false;
                }
                first = matrix[x][y];
            }else if (first > target) {
                x -= 1;
                if (x < 0) {
                    return false;
                }
                first = matrix[x][y];
            }else {
                return true;
            }
        }

    }
}