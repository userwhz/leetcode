package com.whzcloud.offer1;

import java.util.HashMap;

public class Offer10_2 {
    public static void main(String[] args) {
        System.out.println(new SolutionO10_2().numWays(2));
    }
}


class SolutionO10_2 {
    public int numWays(int n) {
        final int MOD = 1000000007;
        if (n == 0) {
            return 1;
        }else if (n == 1) {
            return 1;
        }
        int pre = 1;
        int cur = 1;
        int res = 0;
        for (int i = 2; i <= n; i++) {
            res = (pre + cur) % MOD;
            pre = cur;
            cur = res;
        }
        return res;
    }
}