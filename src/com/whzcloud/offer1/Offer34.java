package com.whzcloud.offer1;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class Offer34 {


}

class SolutionO34 {
    LinkedList<List<Integer>> lists = new LinkedList<>();
    LinkedList<Integer> path = new LinkedList<>();

    public List<List<Integer>> pathSum(TreeNode root, int target) {
        recur(root, target);
        return lists;
    }

    private void recur(TreeNode root, int target) {
        if (root == null) {
            return;
        }
        path.add(root.val);
        target -= root.val;
        if (target == 0 && root.left == null && root.right == null) {
            lists.add(new ArrayList<>(path));
        }
        recur(root.left, target);
        recur(root.right, target);
        path.removeLast();
    }
}