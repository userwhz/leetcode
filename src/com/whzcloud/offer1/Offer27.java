package com.whzcloud.offer1;

import sun.reflect.generics.tree.Tree;

import java.util.LinkedList;
import java.util.Queue;

public class Offer27 {
}

class SolutionO27 {
    public TreeNode mirrorTree(TreeNode root) {
        if (root == null) {
            return null;
        }
        TreeNode left = mirrorTree(root.left);
        TreeNode right= mirrorTree(root.right);
        root.left = right;
        root.right = left;
        return root;


    }
}