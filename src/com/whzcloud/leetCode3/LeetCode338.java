package com.whzcloud.leetCode3;

public class LeetCode338 {
    public static void main(String[] args) {

    }
}

class Solution338 {
    public int[] countBits(int n) {
        int[] bits = new int[n + 1];
        for (int i = 1; i <= n; i++) {
            bits[i] = bits[i & (i - 1)] + 1;
        }
        return bits;
    }

}
