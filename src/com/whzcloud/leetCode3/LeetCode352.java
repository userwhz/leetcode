package com.whzcloud.leetCode3;

import javax.jnlp.ClipboardService;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.TreeMap;

public class LeetCode352 {
    public static void main(String[] args) {
        SummaryRanges sm = new SummaryRanges();
        sm.addNum(1);
        sm.addNum(3);
        sm.addNum(2);
        int[][] res = sm.getIntervals();
        for (int i = 0; i < res.length; i++) {
            for (int j = 0; j < res[0].length; j++) {
                System.out.print(res[i][j]);
            }
            System.out.println();
        }
    }
}

class SummaryRanges {
    private TreeMap<Integer, Integer> tm;// TreeMap<区间开始,区间结束>

    public SummaryRanges() {
        tm = new TreeMap<>();
    }

    public void addNum(int val) {
        Integer floor = tm.floorKey(val);
        Integer floorEnd = floor != null ? tm.get(floor) : null;
        Integer ceil = tm.ceilingKey(val);
        Integer ceilEnd = ceil != null ? tm.get(ceil) : null;
        if (ceil != null && floor != null) {
            if (floorEnd + 1 == val && val + 1 == ceil) {
                tm.put(floor, ceilEnd);
                tm.remove(ceil);
            } else if (floorEnd + 1 == val) {
                tm.put(floor, val);
            } else if (val + 1 == ceil) {
                tm.put(val, ceilEnd);
                tm.remove(ceil);
            } else if (val > floorEnd + 1 && val < ceil - 1) {
                tm.put(val, val);
            }
        } else if (ceil != null) {
            if (val + 1 == ceil) {
                tm.put(val, ceilEnd);
                tm.remove(ceil);
            } else if (val < ceil - 1) {
                tm.put(val, val);
            }
        } else if (floor != null) {
            if (floorEnd + 1 == val) {
                tm.put(floor, val);
            } else if (val > floorEnd + 1) {
                tm.put(val, val);
            }
        } else {
            tm.put(val, val);
        }

    }

    public int[][] getIntervals() {
        int size = tm.size();
        int[][] ans = new int[size][2];
        int p = 0;
        for (Integer start : tm.keySet()) {
            ans[p][0] = start;
            ans[p][1] = tm.get(start);
            p++;
        }
        return ans;
    }
}
