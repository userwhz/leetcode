package com.whzcloud.leetCode3;

public class LeetCode342 {
    public static void main(String[] args) {
        Solution342 solution = new Solution342();
        System.out.println(solution.isPowerOfFour(5));
    }
}

class Solution342 {
    public boolean isPowerOfFour(int n) {
        return n > 0 && (n & (n - 1)) == 0 && (n & 0xaaaaaaaa) == 0;
    }
}
