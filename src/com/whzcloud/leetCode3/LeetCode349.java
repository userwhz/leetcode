package com.whzcloud.leetCode3;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class LeetCode349 {
    public static void main(String[] args) {
        int[] num1 = {1,2,2,1};
        int[] num2 = {2,2};
        Solution349 solution = new Solution349();
        int[] res = solution.intersection(num1, num2);
        System.out.println(Arrays.toString(res));
    }
}

class Solution349 {
    public int[] intersection(int[] nums1, int[] nums2) {
        Set<Integer> set1 = new HashSet<>();
        Set<Integer> set2 = new HashSet<>();
        for(int i : nums1){
            set1.add(i);
        }
        for(int i : nums2){
            if(set1.contains(i)){
                set2.add(i);
            }
        }
        int[] arr = new int[set2.size()];
        int j = 0;
        for(int i : set2){
            arr[j] = i;
            j++;
        }
        return arr;
    }
}