package com.whzcloud.leetCode3;

public class LeetCode300  {
    public static void main(String[] args) {
        int[] nums = {10,9,2,5,3,7,101,18};
        // 1 1 1 2 2 3 4 4
        System.out.println(new Solution300().lengthOfLIS(nums));
    }
}

class Solution300 {
    public int lengthOfLIS(int[] nums) {
        int len = nums.length;
        if (len == 0) {
            return 0;
        }
        int[] dp = new int[len];
        int max = 1;
        dp[0] = 1;
        for (int i = 1; i < len; i++) {
            dp[i] = 1;
            for (int j = 0; j < i; j++) {
                if (nums[j] < nums[i]) {
                    dp[i] = Math.max(dp[i], dp[j] + 1);
                }
            }
            max = Math.max(dp[i], max);
        }
        return max;
    }
}
