package com.whzcloud.leetCode3;

import java.util.Arrays;

public class LeetCode389 {
    public static void main(String[] args) {
        System.out.println(new Solution389().findTheDifference("abcd", "abcde"));
    }
}

class Solution389 {
    public char findTheDifference(String s, String t) {
        int[] count = new int[26];
        for (int i = 0; i < s.length(); i++){
            char ch = s.charAt(i);
            count[ch - 'a']++;
        }
        for (int i = 0; i < t.length(); ++i) {
            char ch = t.charAt(i);
            count[ch - 'a']--;
            if (count[ch - 'a'] < 0) {
                return ch;
            }
        }
        return ' ';
    }
}