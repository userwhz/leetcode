package com.whzcloud.leetCode3;

import java.util.Arrays;
import java.util.Stack;

public class LeetCode344 {
    public static void main(String[] args) {
        char[] s = {'h','e','l','l','o'};
        new Solution344().reverseString(s);
        System.out.println(Arrays.toString(s));
    }
}

class Solution344 {
    public void reverseString(char[] s) {
        Stack<Character> stack = new Stack<>();
        for (char ch : s) {
            stack.push(ch);
        }
        for (int i = 0; i < s.length; i++) {
            s[i] = stack.pop();
        }
    }
}
