package com.whzcloud.leetCode10;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Queue;

public class LeetCode1091 {
    public static void main(String[] args) {
        int[][] grid = {{1,0,0},{1,1,0},{1,1,0}};
        for (int i = 0; i < grid.length; i++) {
            for (int j = 0; j < grid[0].length; j++) {
                System.out.print(grid[i][j]);
            }
            System.out.println();
        }
        System.out.println(new Solution1091().shortestPathBinaryMatrix(grid));
        for (int i = 0; i < grid.length; i++) {
            for (int j = 0; j < grid[0].length; j++) {
                System.out.print(grid[i][j]);
            }
            System.out.println();
        }
    }
}

class Solution1091 {
    int[] dx = {-1, -1, -1, 0, 0, 1, 1, 1};
    int[] dy = {-1, 0, 1, -1, 1, -1, 0, 1};
    public int shortestPathBinaryMatrix(int[][] grid) {
        int x = grid.length;
        int y = grid[0].length;
        if (grid[0][0] == 1 || grid[x - 1][y - 1] == 1){
            return -1;
        }
        if (x == 1 && y == 1 && grid[0][0] == 0) {
            return 1;
        }
        grid[0][0] = 1;
        int res = 1;

        Queue<int[]> queue = new LinkedList();
        queue.add(new int[]{0, 0});
        while (!queue.isEmpty()) {
            int len = queue.size();
            res++;
            System.out.println(res);
            for (int i = 0; i < len; i++) {
                int[] cell = queue.poll();
                int r0 = cell[0];
                int c0 = cell[1];

                for (int j = 0; j < 8; j++) {
                    int r1 = r0 + dx[j];
                    int c1 = c0 + dy[j];
                    if (r1 == x - 1 && c1 == y - 1 ) {
                        return res++;
                    }
                    if (r1 >= 0 && r1 < x && c1 >= 0 && c1 < y && grid[r1][c1] == 0) {
                        System.out.println(r1 + " " + c1);
                        queue.add(new int[]{r1, c1});
                        grid[r1][c1] = 1;
                    }
                }
            }
        }
        return -1;
    }
}