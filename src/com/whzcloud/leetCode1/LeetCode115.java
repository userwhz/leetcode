package com.whzcloud.leetCode1;

public class LeetCode115 {
    public static void main(String[] args) {
        System.out.println(new Solution115().numDistinct("rabbbit", "rabbit"));
    }
}

class Solution115 {
    public int numDistinct(String s, String t) {
        int slen = s.length();
        int tlen = t.length();

        int[][] dp = new int[tlen + 1][slen + 1];
        for (int i = 1; i < tlen + 1; i++) {
            dp[i][0] = 0;
        }
        for (int i = 0; i < slen + 1; i++) {
            dp[0][i] = 1;
        }
        for (int i = 1; i < tlen + 1; i++) {
            for (int j = 1; j < slen + 1; j++) {
                if (t.charAt(i - 1) != s.charAt(j - 1)) {
                    dp[i][j] = dp[i][j - 1];
                }else {
                    dp[i][j] = dp[i - 1][j - 1] + dp[i][j - 1];
                }
            }
        }
        return dp[tlen][slen];
    }
}