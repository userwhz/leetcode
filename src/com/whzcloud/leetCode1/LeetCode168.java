package com.whzcloud.leetCode1;

public class LeetCode168 {
    public static void main(String[] args) {
        System.out.println(new Solution168().convertToTitle(12));

    }
}

class Solution168 {
    public String convertToTitle(int columnNumber) {
        StringBuffer sb = new StringBuffer();
        int remainder;
        while (columnNumber !=0){

            if (columnNumber % 26 == 0) {
                remainder = 26;
            }else {
                remainder = columnNumber % 26;
            }
            char res = (char) (64 + remainder);
            sb.insert(0, res);
            columnNumber = (columnNumber - remainder) / 26;
        }
        return sb.toString();
    }
}
