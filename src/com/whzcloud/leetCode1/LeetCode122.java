package com.whzcloud.leetCode1;

public class LeetCode122 {
    public static void main(String[] args) {

    }
}
class Solution122 {
    public int maxProfit(int[] prices) {
        int profit = 0;
        for (int i = 1; i < prices.length; i++) {
            int temp = prices[i] - prices[i - 1];
            if (temp > 0) {
                profit += temp;
            }
        }
        return profit;
    }
}