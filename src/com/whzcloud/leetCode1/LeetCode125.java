package com.whzcloud.leetCode1;

public class LeetCode125 {
    public static void main(String[] args) {
        System.out.println(new Solution125().isPalindrome(""));
    }
}

class Solution125 {
    public boolean isPalindrome(String s) {
        StringBuffer sb = new StringBuffer();
        int length = s.length();
        for (int i = 0; i < length; i++) {
            char ch = s.charAt(i);
            if (Character.isLetterOrDigit(ch)) {
                sb.append(Character.toLowerCase(ch));
            }
        }
        if (sb.length() == 0){
            return true;
        }
        int right = sb.length() - 1;
        for (int left = 0; left < sb.length() / 2; left++){
            if (Character.toLowerCase(sb.charAt(left)) != Character.toLowerCase(sb.charAt(right))) {
                return false;
            }
            right--;
        }
        return true;
    }
}
