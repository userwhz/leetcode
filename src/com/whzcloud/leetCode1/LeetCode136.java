package com.whzcloud.leetCode1;

import java.util.Arrays;

public class LeetCode136 {
    public static void main(String[] args) {
        int[] nums = {4,1,2,1,2};
        System.out.println(new Solution136().singleNumber(nums));
    }
}

class Solution136 {
    public int singleNumber(int[] nums) {
        Arrays.sort(nums);
        int count = 1;
        for (int i = 1; i < nums.length; i++) {
            if (nums[i] != nums[i - 1]){
                if (count == 1){
                    return nums[i - 1];
                }
                count = 1;
            }else{
                count++;
            }
        }
        return nums[nums.length - 1];
    }
}
