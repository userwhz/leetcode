package com.whzcloud.leetCode1;

import java.util.Arrays;

public class LeetCode169 {
    public static void main(String[] args) {

    }
}

class Solution169 {
    public int majorityElement(int[] nums) {
        Arrays.sort(nums);
        return nums[nums.length / 2];
    }
}

