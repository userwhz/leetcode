package com.whzcloud.leetCode1;

public class LeetCode104 {
    public static void main(String[] args) {

    }
}

class Solution104 {
    public int maxDepth(TreeNode root) {
        //terminator
        if (root == null) {
            return 0;
        }
        //process current logic



        //drill down
        int maxLeft = maxDepth(root.left) + 1;
        int maxRight = maxDepth(root.right) + 1;
        //restore current status
        return Math.max(maxLeft, maxRight);
    }
}
