package com.whzcloud.leetCode1;

import java.util.ArrayList;
import java.util.List;

public class LeetCode118 {
    public static void main(String[] args) {
        Solution118 solution = new Solution118();
        System.out.println(solution.generate(5));
    }
}

class Solution118 {
    public List<List<Integer>> generate(int numRows) {
        List<List<Integer>> lists = new ArrayList<>();
        for (int i = 1; i <= numRows; i++) {
            System.out.println(i);
            List<Integer> list = new ArrayList<>();
            for (int j = 0; j < i; j++){
                if (j == 0 || j == i - 1) {
                    list.add(1);
                }else {
                    list.add(lists.get(i - 2).get(j - 1) + lists.get(i - 2).get(j));
                }
            }
            lists.add(list);
        }
        return lists;
    }
}
