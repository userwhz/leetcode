package com.whzcloud.leetCode1;

import java.util.HashSet;
import java.util.Set;

public class LeetCode128 {

}

class Solution128 {
    public int longestConsecutive(int[] nums) {
        Set<Integer> set = new HashSet<>();
        for (int num: nums) {
            set.add(num);
        }
        int max = 0;
        for (int i = 0; i < nums.length; i++) {
            if (!set.contains(nums[i] - 1)){
                int cur = nums[i];
                int len = 1;
                while (set.contains(cur + 1)) {
                    len++;
                    cur++;
                }
                max = Math.max(max, len);
            }
        }
        return max;
    }
}
