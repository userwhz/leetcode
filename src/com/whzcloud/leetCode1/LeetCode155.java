package com.whzcloud.leetCode1;

import java.util.Deque;
import java.util.LinkedList;
import java.util.Stack;

public class LeetCode155 {
    public static void main(String[] args) {

    }
}

class MinStack155 {
    Stack<Integer> stackA;
    Stack<Integer> stackB;

    public MinStack155() {
        stackA = new Stack<>();
        stackB = new Stack<>();
    }

    public void push(int x) {
        stackA.push(x);
        if (stackB.isEmpty() || stackB.peek() > x) {
            stackB.push(x);
        } else {
            stackB.push(stackB.peek());
        }
    }

    public void pop() {
        stackA.pop();
        stackB.pop();
    }

    public int top() {
        if (!stackA.isEmpty()) {
            return stackA.peek();
        }
        throw new RuntimeException("栈为空");
    }

    public int getMin() {
        return stackB.peek();
    }
}
