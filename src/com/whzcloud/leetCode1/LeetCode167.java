package com.whzcloud.leetCode1;

import java.util.HashMap;

public class LeetCode167 {
    public static void main(String[] args) {

    }
}

class Solution167 {
    public int[] twoSum(int[] numbers, int target) {
        HashMap<Integer, Integer> map = new HashMap<>();
        for (int i = 0; i < numbers.length; i++) {
            if (map.containsKey(numbers[i])){
                return new int[]{map.get(numbers[i]), i + 1};
            }
            int temp = target - numbers[i];
            map.put(temp, i + 1);
        }
        return null;
    }
}
