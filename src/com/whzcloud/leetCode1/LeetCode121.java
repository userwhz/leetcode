package com.whzcloud.leetCode1;

public class LeetCode121 {
    public static void main(String[] args) {
        int[] prices = {7,1,5,3,6,4};
        System.out.println(new Solution121().maxProfit(prices));
    }
}

class Solution121 {
    public int maxProfit(int[] prices) {
        int minprice = Integer.MAX_VALUE;
        int max = 0;
        for (int i = 0; i < prices.length; i++) {
            if (prices[i] < minprice) {
                minprice = prices[i];
            }else if(prices[i] - minprice > max){
                max = prices[i] - minprice;
            }
        }
        return max;
    }
}
