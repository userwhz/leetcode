package com.whzcloud.leetCode1;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class LeetCode187 {
    public static void main(String[] args) {
        System.out.println("ab".substring(0,1));
        System.out.println(new Solution187().findRepeatedDnaSequences("AAAAAAAAAAA"));
    }
}
class Solution187 {
    public List<String> findRepeatedDnaSequences(String s) {
        Set<String> stringSet = new HashSet<>();
        List<String> list = new ArrayList<>();
        for (int i = 0; i < s.length() - 9; i++) {
            String sub = s.substring(i, i + 10);

            if (stringSet.contains(sub)){
                System.out.println(sub);
                if (!list.contains(sub)){
                    list.add(sub);
                }
            }else {
                stringSet.add(sub);
            }
        }
        return list;
    }
}
