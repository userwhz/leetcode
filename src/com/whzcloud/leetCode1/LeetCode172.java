package com.whzcloud.leetCode1;

public class LeetCode172 {
    public static void main(String[] args) {
        System.out.println(new Solution172().trailingZeroes(13));
    }
}

class Solution172 {
    public int trailingZeroes(int n) {
        int count = 0;
        for (int i = 1; i <= n; i++) {
            int N = i;
            while (N > 0) {
                if (N % 5 == 0) {
                    count++;
                    N /= 5;
                } else {
                    break;
                }
            }
        }
        return count;

    }
}
