package com.whzcloud.leetCode1;

public class LeetCode111 {
    public static void main(String[] args) {

    }
}

class Solution111 {
    public int minDepth(TreeNode root) {
        if (root == null) {
            return 0;
        }
        if (root.left == null) {
            return minDepth(root.right) + 1;
        }if (root.right == null) {
            return minDepth(root.left) + 1;
        }else{
            return Math.min(minDepth(root.left), minDepth(root.right)) + 1;
        }
    }
}