package com.whzcloud.leetCode1;

public class LeetCode190 {
    public static void main(String[] args) {
    }
}

class Solution190 {
    public int reverseBits(int n) {
        int ans = 0;
        for (int i = 0; i < 32; i++) {
            int t = (n >> i) & 1;
            if (t == 1) {
                ans |= (1 << (31 - i));
            }
        }
        return ans;
    }
}
