package com.whzcloud.leetCode1;

public class LeetCode171 {
    public static void main(String[] args) {
        System.out.println(new Solution171().titleToNumber("AB"));
    }
}

class Solution171 {
    public int titleToNumber(String columnTitle) {
        int res = 0;
        for (int i = 0; i < columnTitle.length(); i++){
            res = res * 26 + columnTitle.charAt(i) - 64;
        }
        return res;
    }
}
