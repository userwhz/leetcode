package com.whzcloud.leetCode1;

import java.util.ArrayList;
import java.util.List;

public class LeetCode119 {
    public static void main(String[] args) {
        Solution119 solution = new Solution119();
        System.out.println(solution.getRow(3));
    }
}

class Solution119 {
    public List<Integer> getRow(int rowIndex) {
        List<List<Integer>> lists = new ArrayList<>();
        for (int i = 1; i <= rowIndex + 1; i++) {
            List<Integer> list = new ArrayList<>();
            for (int j = 0; j < i; j++){
                if (j == 0 || j == i - 1) {
                    list.add(1);
                }else {
                    list.add(lists.get(i - 2).get(j - 1) + lists.get(i - 2).get(j));
                }
            }
            lists.add(list);
        }
        return lists.get(rowIndex);
    }
}
