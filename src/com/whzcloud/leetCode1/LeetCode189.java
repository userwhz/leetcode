package com.whzcloud.leetCode1;

public class LeetCode189 {
    public static void main(String[] args) {
        int[] nums = {1,2,3,4,5,6,7};
        Solution189 solution = new Solution189();
        solution.rotate(nums, 3);
        for (int i = 0; i < nums.length; i++) {
            System.out.println(nums[i]);
        }
    }
}





class Solution189 {
    public void rotate(int[] nums, int k) {
        int len = nums.length;
        k %= len;
        int[] res = new int[len];
        for (int i = len - k, j = 0; i < len; i++, j++){
            res[j] = nums[i];
        }
        for (int i = k, j = 0; i < len; i++, j++) {
            res[i] = nums[j];
        }
        System.arraycopy(res, 0, nums, 0, len);
    }
}


//通过三次反转
//class Solution189 {
//    public void rotate(int[] nums, int k) {
//        int len = nums.length;
//        k %= len;
//        reverse(nums, 0, len - 1);
//        reverse(nums, 0, k - 1);
//        reverse(nums, k, len - 1);
//    }
//
//    public void reverse(int[] nums, int left, int right) {
//        while (left < right) {
//            int temp = nums[left];
//            nums[left] = nums[right];
//            nums[right] = temp;
//            left++;
//            right--;
//        }
//    }
//
//}