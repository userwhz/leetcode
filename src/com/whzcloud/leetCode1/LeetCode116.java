package com.whzcloud.leetCode1;

import java.util.LinkedList;
import java.util.Queue;

public class LeetCode116 {
    public static void main(String[] args) {

    }
}


class Solution116 {
    public Node connect(Node root) {
        if (root == null) {
            return null;
        }
        Queue<Node> queue = new LinkedList<>();
        queue.add(root);
        while (!queue.isEmpty()) {
            int len = queue.size() - 1;
            Node temp1 = queue.poll();
            if (temp1.left != null) {
                queue.add(temp1.left);
            }
            if (temp1.right != null) {
                queue.add(temp1.right);
            }
            for (int i = 0; i < len; i++) {
                Node temp2 = queue.poll();
                if (temp2.left != null) {
                    queue.add(temp2.left);
                }
                if (temp2.right != null) {
                    queue.add(temp2.right);
                }
                temp1.next = temp2;
                temp1 = temp2;
            }
            temp1.next = null;
        }
        return root;
    }
}


class Node {
    public int val;
    public Node left;
    public Node right;
    public Node next;

    public Node() {}

    public Node(int _val) {
        val = _val;
    }

    public Node(int _val, Node _left, Node _right, Node _next) {
        val = _val;
        left = _left;
        right = _right;
        next = _next;
    }
};
