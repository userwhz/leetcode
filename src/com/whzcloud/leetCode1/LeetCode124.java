package com.whzcloud.leetCode1;

public class LeetCode124 {
    public static void main(String[] args) {

    }
}

//DFS
class Solution1224 {

    int maxRes = Integer.MIN_VALUE;

    public int maxPathSum(TreeNode root) {
        postOrder(root);
        return maxRes;
    }

    /*
        拿到一边，包括root的最大sum
    */
    private int postOrder(TreeNode root) {
        if (root == null) {
            return 0;
        }
        int left = Math.max(postOrder(root.left), 0);
        int right = Math.max(postOrder(root.right), 0);

        maxRes = Math.max(maxRes, root.val + left + right);

        return root.val + Math.max(left, right);
    }


}
