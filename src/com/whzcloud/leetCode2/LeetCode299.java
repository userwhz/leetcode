package com.whzcloud.leetCode2;

import java.util.HashMap;
import java.util.Map;

public class LeetCode299 {
}

class Solution299 {
    public String getHint(String secret, String guess) {
        int x = 0;
        int y = 0;
        int[] cnt1 = new int[10];
        int[] cnt2 = new int[10];
        for (int i = 0; i < secret.length(); i++) {
            int c1 = secret.charAt(i) - '0';
            int c2 = guess.charAt(i) - '0';
            if (c1 == c2) {
                x++;
            }else {
                cnt1[c1]++;
                cnt2[c2]++;
            }
        }
        for (int i = 0; i < 10; i++) {
            y += Math.min(cnt1[i], cnt2[i]);
        }
        return x + "A" + y + "B";
    }
}