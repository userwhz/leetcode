package com.whzcloud.leetCode2;

import java.util.HashMap;

public class LeetCode205 {
    public static void main(String[] args) {
//        "badc"
//        "baba"
    }
}

class Solution205 {
    public boolean isIsomorphic(String s, String t) {
        HashMap<Character, Character> map1 = new HashMap<>();
        HashMap<Character, Character> map2 = new HashMap<>();
        for (int i = 0; i < s.length(); i++){
            char x = s.charAt(i), y = t.charAt(i);
            if ((map1.containsKey(x) && map1.get(x) != y) || (map2.containsKey(y) && map2.get(y) != x)) {
                return false;
            }
            map1.put(x, y);
            map2.put(y, x);

        }
        return true;
    }
}