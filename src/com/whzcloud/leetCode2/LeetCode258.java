package com.whzcloud.leetCode2;

import java.util.ArrayList;

public class LeetCode258 {
    public static void main(String[] args) {
        System.out.println(new Solution258().addDigits(38));
    }
}

class Solution258 {
    public int addDigits(int num) {
        while (num >= 10) {
            int sum = 0;
            ArrayList<Integer> digits = new ArrayList<>();

            while (num != 0) {
                digits.add(num % 10);
                num /= 10;
            }

            for (int i = 0; i < digits.size(); i++) {
                sum += digits.get(i);
            }

            num = sum;
        }

        return num;
    }
}
