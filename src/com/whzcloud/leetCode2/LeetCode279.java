package com.whzcloud.leetCode2;

public class LeetCode279 {
    public static void main(String[] args) {
        System.out.println(new Solution279().numSquares(5));
    }
}


class Solution279 {
    public int numSquares(int n) {
        int dp[] = new int[n + 1];
        for (int i = 1; i <= n; i++){
            dp[i] = i;
            for (int j = 1; i - j * j >= 0; j++){
                dp[i] = Math.min(dp[i], dp[i - j * j] + 1);
            }
        }
        return dp[n];
    }
}

