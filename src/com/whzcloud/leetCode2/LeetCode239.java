package com.whzcloud.leetCode2;

import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Deque;
import java.util.Queue;

public class LeetCode239 {
    public static void main(String[] args) {
        int[] nums = {1,3,-1,-3,5,3,6,7};
        Solution239 solution = new Solution239();
        System.out.println(Arrays.toString(solution.maxSlidingWindow(nums, 3)));
    }
}

class Solution239 {
    public int[] maxSlidingWindow(int[] nums, int k) {
        if(nums.length == 0 || k == 0) {
            return new int[0];
        }
        Deque<Integer> deque = new ArrayDeque<>();
        int n = nums.length;
        int[] res = new int[n - k + 1];
        for (int i = 0, j = 0; i < n; i++) {
            while (!deque.isEmpty() && i - k + 1 > deque.getFirst()) {
                deque.pollFirst();
            }
            while (!deque.isEmpty() && nums[i] > nums[deque.getLast()]) {
                deque.pollLast();
            }
            deque.offer(i);
            if (i - k + 1 >= 0) {
                res[j] = nums[deque.getFirst()];
                j++;
            }
        }
        return res;
    }
}