package com.whzcloud.leetCode2;

public class LeetCode203 {
    public static void main(String[] args) {

    }
}

class Solution203 {
    public ListNode removeElements(ListNode head, int val) {
        ListNode cur = new ListNode(val - 1);
        cur.next = head;
        ListNode pre = cur;
        while(pre.next != null){
            if(pre.next.val == val){
                pre.next = pre.next.next;
            }else{
                pre = pre.next;
            }
        }
        return cur.next;
    }
}
