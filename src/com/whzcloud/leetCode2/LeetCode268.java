package com.whzcloud.leetCode2;

import java.util.Arrays;

public class LeetCode268 {
}

class Solution268 {
    public int missingNumber(int[] nums) {
        Arrays.sort(nums);
        for (int i = 0; i < nums.length; i++) {
            if (nums[i] == i) {
                continue;
            }else {
                return i;
            }
        }
        return nums.length;
    }
}