package com.whzcloud.leetCode2;

import java.util.Arrays;

public class LeetCode283 {
    public static void main(String[] args) {
        int[] nums = {0,1,0,3,12};
        new Solution283().moveZeroes(nums);
        System.out.println(Arrays.toString(nums));
    }
}

//1.统计0个数
//2.开启新数组

//O(n)
class Solution283 {
    public void moveZeroes(int[] nums) {
        for (int i = 0, j = 0; i < nums.length; i++) {
            if (nums[i] != 0) {
                nums[j] = nums[i];
                if (i != j) {
                    nums[i] = 0;
                }
                j++;
            }
        }
    }
}