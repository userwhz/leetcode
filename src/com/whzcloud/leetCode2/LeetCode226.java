package com.whzcloud.leetCode2;


import sun.reflect.generics.tree.Tree;

import java.util.LinkedList;
import java.util.Queue;

public class LeetCode226 {
    public static void main(String[] args) {

    }
}


class Solution226 {
    //迭代解决
    public TreeNode invertTree(TreeNode root) {
       if (root == null) {
           return null;
       }
       Queue<TreeNode> queue = new LinkedList<>();
       queue.add(root);
       while (!queue.isEmpty()) {
           TreeNode temp = queue.poll();
           TreeNode left = temp.left;
           temp.left = temp.right;
           temp.right = left;
           if (temp.left != null) {
               queue.add(temp.left);
           }
           if (temp.right != null) {
               queue.add(temp.right);
           }
       }
       return root;
    }
}

//
//class TreeNode {
//    int val;
//    TreeNode left;
//    TreeNode right;
//
//    TreeNode() {
//    }
//
//    TreeNode(int val) {
//        this.val = val;
//    }
//
//    TreeNode(int val, TreeNode left, TreeNode right) {
//        this.val = val;
//        this.left = left;
//        this.right = right;
//    }
//}
