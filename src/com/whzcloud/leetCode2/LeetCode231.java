package com.whzcloud.leetCode2;

public class LeetCode231 {

}


class Solution231 {
    public boolean isPowerOfTwo(int n) {
        if (n <= 0) {
            return false;
        }
        n = n & (n - 1);
        if (n != 0) {
            return false;
        }
        return true;
    }
}