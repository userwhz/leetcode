package com.whzcloud.leetCode4;

public class LeetCode441 {
    public static void main(String[] args) {
        System.out.println(new Solution441().arrangeCoins(0));
        System.out.println(3 + 1 >> 1);
    }
}


class Solution441{
    public int arrangeCoins(int n) {
        int i = 0;
        while (n > 0) {
            i++;
            n -= i;
        }
        return n == 0 ? i : i - 1;
    }
}