package com.whzcloud.leetCode4;

public class LeetCode415 {
    public static void main(String[] args) {
        System.out.println(new Solution415().addStrings("584", "18"));
    }
}

class Solution415 {
    public String addStrings(String num1, String num2) {
        int carry = 0;
        int temp = 0;
        StringBuffer sb1 = new StringBuffer(num1);
        StringBuffer sb2 = new StringBuffer(num2);
        StringBuffer res = new StringBuffer();
        sb1.reverse();
        sb2.reverse();
        for (int i = 0; i < Math.min(sb1.length(), sb2.length()); i++) {
            temp = (sb1.charAt(i) - '0' + sb2.charAt(i) - '0' + carry) % 10;
            res.append(temp);
            carry = (sb1.charAt(i) - '0' + sb2.charAt(i) - '0' + carry) / 10;
        }
        boolean flag = true;
        if (sb1.length() < sb2.length()) {
            flag = false;
        }else if (sb1.length() == sb2.length()){
            if (carry == 1){
                res.append(carry);
                return res.reverse().toString();
            }else {
                return res.reverse().toString();
            }
        }
        for (int i = Math.min(sb1.length(), sb2.length()); i < Math.max(sb1.length(), sb2.length()); i++){
            if (flag){
                if (sb1.charAt(i) - '0' + carry == 10){
                    res.append(0);
                    carry = 1;
                } else {
                    res.append(sb1.charAt(i) - '0' + carry);
                    carry = 0;
                }
            }else {
                if (sb2.charAt(i) - '0' + carry == 10){
                    res.append(0);
                    carry = 1;
                }else {
                    res.append(sb2.charAt(i) - '0' + carry);
                    carry = 0;
                }
            }
        }
        if (carry == 1){
            res.append(1);
        }
        return res.reverse().toString();
    }
}
