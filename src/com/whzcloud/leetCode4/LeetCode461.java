package com.whzcloud.leetCode4;

public class LeetCode461 {
    public static void main(String[] args) {
        Solution461 solution = new Solution461();
        System.out.println(solution.hammingDistance(1, 4));
    }
}

class Solution461 {
    public int hammingDistance(int x, int y) {
        int ans = 0;
        for (int i = 0; i < 32; i++){
            int a = (x >> i) & 1;
            int b = (y >> i) & 1;
            ans += a ^ b;
        }
        return ans;
    }
}
