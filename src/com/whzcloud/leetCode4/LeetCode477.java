package com.whzcloud.leetCode4;

public class LeetCode477 {
    public static void main(String[] args) {
        Solution477 solution = new Solution477();
        int[] nums = {4, 14, 2};
        System.out.println(solution.totalHammingDistance(nums));
    }
}

class Solution477 {
    public int totalHammingDistance(int[] nums) {
        int res = 0;
        for (int i = 0; i < 32; i++) {
            int a = 0;
            int b = 0;
            for (int num : nums) {
                int tmp = num >> i;
                if ((tmp & 1) == 1) {
                    a++;
                } else {
                    b++;
                }
            }
            res += a * b;
        }
        return res;
    }
}
