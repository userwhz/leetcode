package com.whzcloud.leetCode4;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class LeetCode438 {
    public static void main(String[] args) {
        System.out.println('a' - 'a');
    }
}


class Solution438 {
    public List<Integer> findAnagrams(String s, String p) {
        int n = s.length();
        int m = p.length();
        int[] pCnt = new int[26];
        int[] sCnt = new int[26];
        List<Integer> res = new ArrayList<>();
        if (n < m) {
            return res;
        }
        for (int i = 0; i < m; i++) {
            pCnt[p.charAt(i) - 'a']++;
            sCnt[s.charAt(i) - 'a']++;
        }
        if(Arrays.equals(sCnt, pCnt)){
            res.add(0);
        }
        // 1 2 3 4
        // 0 1 2 3
        for(int i = m; i < n; i++){
            sCnt[s.charAt(i - m) - 'a']--;
            sCnt[s.charAt(i) - 'a']++;
            if(Arrays.equals(sCnt, pCnt)){
                res.add(i - m + 1);
            }
        }
        return res;

    }

}