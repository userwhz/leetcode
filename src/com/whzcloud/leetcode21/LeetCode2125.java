package com.whzcloud.leetcode21;

public class LeetCode2125 {
    public static void main(String[] args) {

    }
}

class Solution2125 {
    public int search(int[] nums, int target) {
        int len = 0;
        for (int i = 0; i < nums.length; i++) {
            if (nums[i] == target) {
                len++;
            }
            if (nums[i] > target) {
                break;
            }
        }
        return len;
    }
}