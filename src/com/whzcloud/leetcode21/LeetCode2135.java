package com.whzcloud.leetcode21;

public class LeetCode2135 {
    public static void main(String[] args) {
        System.out.println(new Solution2135().reverseLeftWords("abcdefg",2));
    }
}


class Solution2135 {
    public String reverseLeftWords(String s, int n) {
        int len = s.length();
        char[] chars = new char[len];
        for (int i = 0; i < len - n; i++) {
            chars[i] = s.charAt(i + n);
        }
        for (int i = len - n, j = 0; i < len; i++, j++) {
            chars[i] = s.charAt(j);
        }
        StringBuilder sb = new StringBuilder();
        for (char c : chars) {
            System.out.println(c);
            sb.append(c);
        }
        return sb.toString();
    }
}