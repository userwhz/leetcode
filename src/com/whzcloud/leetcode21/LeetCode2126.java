package com.whzcloud.leetcode21;

public class LeetCode2126 {
    public static void main(String[] args) {

    }
}
class Solution2126 {
    public int missingNumber(int[] nums) {
        int left = 0;
        int right = nums.length - 1;
        while (left <= right) {
            int mid = left + (right - left) / 2;
            if (nums[mid] == mid) {
                left = mid + 1;
            }else {
                right = mid - 1;
            }
        }
        return left;
    }
}