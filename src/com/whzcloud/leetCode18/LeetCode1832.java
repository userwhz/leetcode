package com.whzcloud.leetCode18;

public class LeetCode1832 {
    public static void main(String[] args) {
        System.out.println(new Solution1832().checkIfPangram("thequickbrownfoxjumpsoverthelazydog"));
    }
}

class Solution1832 {
    public boolean checkIfPangram(String sentence) {
        int[] count = new int[26];
        for (char c: sentence.toCharArray()){
            count[c - 'a'] += 1;
        }
        for (int i = 0; i < count.length; i++){
            if (count[i] == 0){
                return false;
            }
        }
        return true;
    }
}