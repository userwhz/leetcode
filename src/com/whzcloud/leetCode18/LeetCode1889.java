package com.whzcloud.leetCode18;

public class LeetCode1889 {
    public static void main(String[] args) {
        int[] coins = {4,2,1};
        System.out.println(new Solution1889().minCount(coins));
    }
}

class Solution1889 {
    public int minCount(int[] coins) {
        int res = 0;
        for (int i = 0; i < coins.length; i++){
            res += coins[i] / 2 + coins[i] % 2;
        }
        return res;
    }
}