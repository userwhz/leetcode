package com.whzcloud.leetCode17;

public class LeetCode1744 {
    public static void main(String[] args) {
        Solution1744 solution = new Solution1744();
        int[] candiesCount = {7,4,5,3,8};
        int[][] queries = {{0,2,2}
                            ,{4,2,4}
                            ,{2,13,1000000000}};
        System.out.println(solution.canEat(candiesCount, queries));
    }
}


class Solution1744 {
    public boolean[] canEat(int[] candiesCount, int[][] queries) {
        return new boolean[]{false, true};
    }
}