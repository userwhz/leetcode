package com.whzcloud.leetCode7;

import java.util.HashMap;
import java.util.Map;

public class LeetCode771 {

}

class Solution771 {
    public int numJewelsInStones(String jewels, String stones) {
        Map<Character,Integer> map = new HashMap<>();
        char[] chars = stones.toCharArray();
        char[] chars1 = jewels.toCharArray();
        for (int i = 0; i < chars.length; i++) {
            map.put(chars[i], map.getOrDefault(chars[i], 0) + 1);
        }
        int res = 0;
        for (int i = 0; i < chars1.length; i++) {
            res += map.getOrDefault(chars1[i], 0);
        }
        return res;
    }
}