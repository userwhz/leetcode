package com.whzcloud.leetCode7;

import java.util.logging.XMLFormatter;

public class LeetCode733 {
    public static void main(String[] args) {
        int[][] image = {{0,0,0},
                {0,1,1}};
        int[][] ints = new Solution733().floodFill(image, 1, 1, 2);
        for (int i = 0; i < ints.length; i ++) {
            for (int j = 0; j < ints[0].length; j++) {
                System.out.print(ints[i][j]);
            }
            System.out.println();
        }
    }
}

class Solution733 {
    int[] dx = {0, 0, -1, 1};
    int[] dy = {1, -1, 0, 0};
    public int[][] floodFill(int[][] image, int sr, int sc, int newColor) {
        int currColor = image[sr][sc];
        if (currColor != newColor) {
            dfs(image, sr, sc, image[sr][sc], newColor);
        }

        return image;
    }

    private void dfs(int[][] image, int sr, int sc, int value, int newColor) {
        if (image[sr][sc] != value) {
            return;
        }
        image[sr][sc] = newColor;
        for (int i = 0; i < 4; i++) {
            int x = sr + dx[i];
            int y = sc + dy[i];
            if (x >= 0 && x < image.length && y >= 0 && y < image[0].length) {
                dfs(image, x, y, value, newColor);
            }
        }
    }
}
// 1 2 1
// 2 2 0
// 2 0 1