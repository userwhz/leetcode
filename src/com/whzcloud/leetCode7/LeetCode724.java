package com.whzcloud.leetCode7;

public class LeetCode724 {
    public static void main(String[] args) {
        int[] nums = {1, 7, 3, 6, 5, 6};
        System.out.println(new Solution724().pivotIndex(nums));
    }
}

class Solution724 {
    public int pivotIndex(int[] nums) {
        int start = 0;
        int end = nums.length - 1;
        int left = 0;
        int right = 0;
        for (int i = 0; i < nums.length; i++){
            while (start < i){
                left += nums[start];
                start++;
            }
            while (end > i){
                right += nums[end];
                end--;
            }
            if (left == right){
                return i;
            }
            start = 0;
            end = nums.length - 1;
            left = 0;
            right = 0;
        }
        return -1;
    }
}
