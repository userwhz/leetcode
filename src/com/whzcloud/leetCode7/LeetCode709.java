package com.whzcloud.leetCode7;

public class LeetCode709 {

}

class Solution709 {
    public String toLowerCase(String s) {
        if (s == null || s.length() == 0) {
            return s;
        }
        char[] chars = s.toCharArray();
        for (int i = 0; i < s.length(); i++) {
            if (chars[i] >= 'A' && chars[i] <= 'Z') {
                chars[i] += 32;
            }
        }
        return String.valueOf(chars);

    }
}
