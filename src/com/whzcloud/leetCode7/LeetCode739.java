package com.whzcloud.leetCode7;

import java.util.Arrays;
import java.util.Stack;

public class LeetCode739 {
}


class Solution739 {
    public int[] dailyTemperatures(int[] temperatures) {
        int len = temperatures.length;
        int[] res = new int[len];
        Stack<Integer> stack = new Stack<>();
        for (int i = 0; i < len; i++) {
            while (!stack.isEmpty() && temperatures[i] > temperatures[stack.peek()]) {
                int temp = stack.pop();
                res[temp] = i - temp;
            }
            stack.add(i);
        }
        return res;
    }
}