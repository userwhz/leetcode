package com.whzcloud.leetCode20;

import java.util.Stack;

public class LeetCode2066 {
    public static void main(String[] args) {
    }
}

class CQueue {
    private Stack<Integer> stack1;
    private Stack<Integer> stack2;
    public CQueue() {
        stack1 = new Stack();
        stack2 = new Stack();
    }

    public void appendTail(int value) {
        stack1.push(value);
    }

    public int deleteHead() {
        if (!stack2.isEmpty()){
            return stack2.pop();
        }
        if (stack1.isEmpty()){
            return -1;
        }else if (stack1.size() == 1) {
            return stack1.pop();
        }
        while (stack1.size() != 1) {
            stack2.push(stack1.pop());
        }
        int res = stack1.pop();
        return res;
    }
}