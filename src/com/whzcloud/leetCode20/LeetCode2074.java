package com.whzcloud.leetCode20;

public class LeetCode2074 {
    public static void main(String[] args) {
        System.out.println(new Solution2074().replaceSpace("We are happy."));

    }
}

class Solution2074 {
    public String replaceSpace(String s) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) != ' '){
                sb.append(s.charAt(i));
            }else {
                sb.append("%20");
            }
        }
        return sb.toString();
    }
}