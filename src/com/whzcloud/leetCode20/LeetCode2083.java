package com.whzcloud.leetCode20;

import java.util.Arrays;

public class LeetCode2083 {
    public static void main(String[] args) {
        int[] nums = {9,6,4,2,3,5,7,0,1};
        System.out.println(new Solution2083().missingNumber(nums));
    }
}

class Solution2083 {
    public int missingNumber(int[] nums) {
        int sum = (0 + nums.length) * (nums.length + 1) / 2;
        for (int i = 0; i < nums.length; i++){
            sum -= nums[i];
        }
        return sum;
    }
}
