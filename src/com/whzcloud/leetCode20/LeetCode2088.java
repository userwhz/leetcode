package com.whzcloud.leetCode20;

import java.util.Stack;

public class LeetCode2088 {
    public static void main(String[] args) {

    }
}

class MinStack {
    Stack<Integer> stack1;
    Stack<Integer> stack2;
    /** initialize your data structure here. */
    public MinStack() {
        stack1 = new Stack<>();
        stack2 = new Stack<>();
    }

    public void push(int x) {
        if (stack2.isEmpty()){
            stack2.push(x);
        } else if (stack2.peek() < x) {
            stack2.push(stack2.peek());
        } else {
            stack2.push(x);
        }
        stack1.push(x);
    }

    public void pop() {
        stack2.pop();
        stack1.pop();
    }

    public int top() {
        return stack1.peek();
    }

    public int min() {
        return stack2.peek();
    }
}


