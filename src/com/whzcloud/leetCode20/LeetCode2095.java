package com.whzcloud.leetCode20;

public class LeetCode2095 {
    public static void main(String[] args) {
        int[] nums = {2, 1, 4, 5, 3, 1, 1, 3};
        System.out.println(new Solution2095().massage(nums));
    }
}

class Solution2095 {
    public int massage(int[] nums) {
        if (nums == null || nums.length == 0) return 0;
        if (nums.length == 1) return nums[0];
        if (nums.length == 2) return Math.max(nums[0], nums[1]);
        int[] f = new int[nums.length];
        f[0] = nums[0];
        f[1] = Math.max(f[0], nums[1]);
        for (int n = 2; n < nums.length; n++) {
            f[n] = Math.max(f[n - 2] + nums[n], f[n - 1]);
        }
        return f[nums.length - 1];

    }
}