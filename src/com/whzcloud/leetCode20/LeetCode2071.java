package com.whzcloud.leetCode20;

import java.util.Arrays;
import java.util.Stack;

public class LeetCode2071 {
    public static void main(String[] args) {
        ListNode l1 = new ListNode(1);
        ListNode l2 = new ListNode(3);
        ListNode l3 = new ListNode(2);

        l1.next = l2;
        l2.next = l3;
        System.out.println(Arrays.toString(new Solution2071().reversePrint(l1)));
    }
}

class Solution2071 {
    public int[] reversePrint(ListNode head) {
        int i = 0;
        Stack<Integer> stack = new Stack<>();
        ListNode cur = head;
        while (cur != null){
            stack.push(cur.val);
            cur = cur.next;
            i++;
        }
        int[] res = new int[i];
        for (int j = 0; j < i; j++){
            res[j] = stack.pop();
        }
        return res;

    }
}