package com.whzcloud.leetCode20;

public class LeetCode2054 {
    public static void main(String[] args) {
        String[] words = {"at", "", "", "", "ball", "", "", "car", "", "","dad", "", ""};
        System.out.println(new Solution2054().findString(words, "car"));
    }
}

class Solution2054 {
    public int findString(String[] words, String s) {
        for (int i = 0; i < words.length; i++){
            if (words[i].equals(s)){
                return i;
            }
        }
        return -1;
    }
}
