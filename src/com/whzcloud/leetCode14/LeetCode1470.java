package com.whzcloud.leetCode14;

import java.util.Stack;

public class LeetCode1470 {
    public static void main(String[] args) {
        int[] nums = {2, 5, 1, 3, 4, 7};
        Solution1470 solution = new Solution1470();
        int[] res = solution.shuffle(nums, 3);
        for (int i = 0; i < res.length; i++) {
            System.out.println(res[i]);
        }
    }
}

class Solution1470 {
    public int[] shuffle(int[] nums, int n) {
        int[] num = new int[nums.length];
        int j = 0;
        for (int i = 0; i < num.length; i++) {
            if (i % 2 == 0) {
                num[i] = nums[i / 2];
            } else {
                num[i] = nums[(nums.length / 2) + j];
                j++;
            }
        }
        return num;
    }
}
