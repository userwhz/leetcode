package com.whzcloud.leetCode5;

import java.util.ArrayList;
import java.util.List;

public class LeetCode590 {

}


class Solution590 {
    List<Integer> list = new ArrayList<>();
    public List<Integer> postorder(Node root) {
        if(root == null) {
            return list;
        }
        for (Node node: root.children) {
            postorder(node);
        }
        list.add(root.val);
        return list;
    }
}