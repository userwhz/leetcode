package com.whzcloud.leetCode5;

public class LeetCode518 {
    public static void main(String[] args) {
        int[] coins = {1, 2, 5};
        System.out.println(new Solution518().change(5, coins));
    }
}

class Solution518 {
    public int change(int amount, int[] coins) {
        int[] dp = new int[amount + 1];
        dp[0] = 1;
        for(int i = 0; i < coins.length; i++){
            if(coins[i] > amount) continue;
            for(int j = coins[i]; j <= amount; j++){
                dp[j] += dp[j - coins[i]];
            }
        }
        return dp[amount];
    }
}

