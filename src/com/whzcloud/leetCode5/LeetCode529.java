package com.whzcloud.leetCode5;

import java.util.Arrays;

public class LeetCode529 {
    public static void main(String[] args) {
        char[][] board = {{'E', 'E', 'E', 'E', 'E'},
                {'E', 'E', 'M', 'E', 'E'},
                {'E', 'E', 'E', 'E', 'E'},
                {'E', 'E', 'E', 'E', 'E'}};
        char[][] newBoard = new Solution529().updateBoard(board, new int[]{3, 0});
        for (char[] chars: newBoard) {
            for (char ch: chars){
                System.out.print(ch);
            }
            System.out.println();
        }
    }
}

class Solution529 {
    int[] dx = {0, 1, 0, -1, 1, 1, -1, -1};
    int[] dy = {1, 0, -1, 0, 1, -1, 1, -1};
    public char[][] updateBoard(char[][] board, int[] click) {
        int x = click[0];
        int y = click[1];
        if (board[x][y] == 'M') {
            board[x][y] = 'X';
        }else {
            dfs(board, x, y);
        }
        return board;
    }

    private void dfs(char[][] board, int x, int y) {
        int count = 0;
        for (int i = 0; i < 8; i++) {
            int bx = x + dx[i];
            int by = y + dy[i];
            if (bx < 0 || bx >= board.length || by < 0 || by >= board[0].length) {
                continue;
            }
            if (board[bx][by] == 'M') {
                count++;
            }
        }
        if (count > 0) {
            board[x][y] = (char) (count + '0');
        }else {
            board[x][y] = 'B';
            for (int i = 0; i < 8; i++) {
                int bx = x + dx[i];
                int by = y + dy[i];
                if (bx < 0 || bx >= board.length || by < 0 || by >= board[0].length|| board[bx][by] != 'E') {
                    continue;
                }
                dfs(board, bx, by);
            }
        }
    }
}