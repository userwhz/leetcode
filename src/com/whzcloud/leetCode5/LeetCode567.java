package com.whzcloud.leetCode5;

import java.util.Arrays;
import java.util.HashMap;

public class LeetCode567 {
    public static void main(String[] args) {
        System.out.println(new Solution567().checkInclusion("adc", "dcda"));
    }
}

class Solution567 {
    public boolean checkInclusion(String s1, String s2) {
        int len1 = s1.length();
        int len2 = s2.length();
        if (len1 > len2) {
            return false;
        }
        int[] cnt1 = new int[26];
        int[] cnt2 = new int[26];
        for (int i = 0; i < len1; i++) {
            cnt1[s1.charAt(i) - 'a']++;
            cnt2[s2.charAt(i) - 'a']++;
        }
        if (Arrays.equals(cnt1, cnt2)){
            return true;
        }
        for (int i = len1; i < len2; i++) {
            cnt2[s2.charAt(i) - 'a']++;
            cnt2[s2.charAt(i - len1) - 'a']--;
            if (Arrays.equals(cnt1, cnt2)){
                return true;
            }
        }
        return false;
    }
}