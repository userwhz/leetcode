package com.whzcloud.leetCode5;

public class LeetCode551 {
    public static void main(String[] args) {
        System.out.println(new Solution551().checkRecord("PPALLL"));
    }
}

class Solution551 {
    public boolean checkRecord(String s) {
        int A = 0;
        int L = 0;
        boolean flag = true;
        for (int i = 0; i < s.length(); i++){
            if (s.charAt(i) == 'A'){
                A++;
                L = 0;
            }else if (s.charAt(i) == 'L'){
                L++;
            }else {
                L = 0;
            }
            if (A > 1 || L > 2){
                flag = false;
                break;
            }
        }
        return flag;
    }
}
