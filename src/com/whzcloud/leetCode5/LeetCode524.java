package com.whzcloud.leetCode5;

import java.util.ArrayList;
import java.util.List;

public class LeetCode524 {
    public static void main(String[] args) {
        Solution524 solution = new Solution524();
        List<String> list = new ArrayList<>();
        list.add("good");
        list.add("best");
        list.add("good");
        list.add("monkey");
        list.add("plea");
        System.out.println(solution.findLongestWord( "wordgoodgoodgoodbestword", list));
        System.out.println('g' < 'b');
    }

}

class Solution524 {
    public String findLongestWord(String s, List<String> dictionary) {
        String res = "";
        int max = 0;
        for (int i = 0; i < dictionary.size(); i++) {
            for (int j = 0, k = 0; j < s.length() && k < dictionary.get(i).length(); j++) {
                int len = dictionary.get(i).length();
                String dic = dictionary.get(i);
                if (s.charAt(j) != dic.charAt(k)) {
                    continue;
                }else {
                    k++;
                }
                if (k == len && k == max) {
                    for (int m = 0; m < len; m++) {
                        if (dic.charAt(m) < res.charAt(m)){
                            res = dic;
                        }else if (dic.charAt(m) == res.charAt(m)){
                            continue;
                        }else {
                            break;
                        }
                    }
                }else if (k == len && k > max) {
                    max = len;
                    res = dic;
                    break;
                }
            }
        }
        return res;
    }
}