package com.whzcloud.leetCode5;

import java.util.LinkedList;
import java.util.Queue;

public class LeetCode542 {
}


class Solution542 {
    int[] dx = {0, 0, -1, 1};
    int[] dy = {1, -1, 0, 0};
    public int[][] updateMatrix(int[][] mat) {
        int x = mat.length;
        int y = mat[0].length;
        boolean[][] seen = new boolean[x][y];
        Queue<int[]> queue = new LinkedList();
        for (int i = 0; i < x; i++) {
            for (int j = 0; j < y; j++) {
                if (mat[i][j] == 0) {
                    queue.add(new int[]{i, j});
                    seen[i][j] = true;
                }
            }
        }

        while (!queue.isEmpty()) {
            int[] cell = queue.poll();
            int r = cell[0];
            int c = cell[1];
            for (int i = 0; i < 4; i++) {
                int ni = r + dx[i];
                int nj = c + dy[i];
                if (ni >= 0 && ni < x && nj >= 0 && nj < y && !seen[ni][nj]) {
                    mat[ni][nj] = mat[r][c] + 1;
                    queue.add(new int[]{ni, nj});
                    seen[ni][nj] = true;
                }

            }
        }
        return mat;
    }


}