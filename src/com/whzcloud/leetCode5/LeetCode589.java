package com.whzcloud.leetCode5;

import java.util.ArrayList;
import java.util.List;

public class LeetCode589 {
    public static void main(String[] args) {


        Node node5 = new Node(5);
        Node node6 = new Node(6);
        List<Node> list2 = new ArrayList<>();
        list2.add(node5);
        list2.add(node6);
        Node node3 = new Node(3, list2);
        Node node2 = new Node(2);
        Node node4 = new Node(4);
        List<Node> list1 = new ArrayList<>();
        list1.add(node3);
        list1.add(node2);
        list1.add(node4);
        Node node1 = new Node(1, list1);

        Solution589 solution = new Solution589();
        List<Integer> res = solution.preorder(node1);
        for (int i : res){
            System.out.println(i);
        }
    }
}

class Solution589 {
    List<Integer> res = new ArrayList<>();
    public List<Integer> preorder(Node root) {
        if (root == null){
            return res;
        }
        for (Node node: root.children){
            preorder(node);
        }
        res.add(root.val);
        return res;
    }

}

class Node {
    public int val;
    public List<Node> children;

    public Node() {}

    public Node(int _val) {
        val = _val;
    }

    public Node(int _val, List<Node> _children) {
        val = _val;
        children = _children;
    }
};
