package com.whzcloud.leetCode5;

import java.util.Arrays;

public class LeetCode557 {
    public static void main(String[] args) {
        System.out.println(new Solution557().reverseWords("Let's take LeetCode contest"));
    }
}


class Solution557 {
    public String reverseWords(String s) {
        String[] split = s.split(" ");
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < split.length; i++) {
            String s1 = split[i];
            char[] chars = s1.toCharArray();
            int left = 0;
            int right = s1.length() - 1;
            while (left < right) {
                Character temp = chars[left];
                chars[left] = chars[right];
                chars[right] = temp;
                left++;
                right--;
            }
            for (char c: chars) {
                sb.append(c);
            }
            sb.append(" ");
        }
        String res =sb.toString();
        return res.substring(0, res.length() - 1);
    }
}