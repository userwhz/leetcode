package com.whzcloud.leetCode13;

import java.util.Stack;

public class LeetCode1323 {
    public static void main(String[] args) {
        System.out.println(new Solution1323().maximum69Number(9669));
    }
}
class Solution1323 {
    public int maximum69Number (int num) {
        Stack<Integer> stack = new Stack<>();
        boolean flag = true;
        int temp = 0;
        while (num != 0){
            temp = num % 10;
            num /= 10;
            stack.push(temp);
        }
        int res = 0;
        while (!stack.isEmpty()){
            if (stack.peek() == 6 && flag == true){
                res = res * 10 + 9;
                stack.pop();
                flag = false;
            } else {
                res = res * 10 + stack.pop();
            }
        }
        return res;
    }
}