package com.whzcloud.dailycode;

import java.util.Stack;

public class Daily_test1 {
    public static void main(String[] args) {
        int[] nums = {52,54,57,41,41,66,70,73};
        int[] visited = new Daily_demo1().visited(nums);
        for (int i : visited) {
            System.out.print(i);
        }
    }
}

class Daily_demo1{
    public int[] visited(int[] nums) {
        int len = nums.length;
        int[] res = new int[len];
        Stack<Integer> stack = new Stack<>();
        for (int i = 0; i < len; i++) {
            while (!stack.isEmpty() && nums[i] > nums[stack.peek()]) {
                int temp = stack.pop();
                res[temp] = i - temp;
            }
            stack.add(i);
        }
        return res;
    }
}