package com.whzcloud.leetCode9;

import java.util.LinkedList;
import java.util.Queue;

public class LeetCode994 {
    public static void main(String[] args) {
        int[][] grid = {{1,2}};
        for (int i = 0; i < grid.length; i++) {
            for (int j = 0; j < grid[0].length; j++) {
                System.out.print(grid[i][j]);
            }
            System.out.println();
        }
        System.out.println("====");
        System.out.println(new Solution994().orangesRotting(grid));

    }

}


class Solution994 {
    public int orangesRotting(int[][] grid) {
        int res = 0;
        Queue<int[]> queue = new LinkedList<>();
        int count1 = 0;
        int count2 = 0;
        for (int i = 0; i < grid.length; i++) {
            for (int j = 0; j < grid[0].length; j++) {
                if (grid[i][j] == 1) {
                    count1++;
                }
                if (grid[i][j] == 2){
                    queue.add(new int[]{i, j});
                }
            }
        }

        while (count1 > 0 && !queue.isEmpty()) {
            count2++;

            int len = queue.size();
            for (int i = 0; i < len; i++) {
                int[] temp = queue.poll();
                int r = temp[0];
                int c = temp[1];
                if (r - 1 >= 0 && grid[r - 1][c] == 1) {
                    grid[r - 1][c] = 2;
                    count1--;
                    queue.add(new int[]{r - 1, c});
                }
                if (c - 1 >= 0 && grid[r][c - 1] == 1) {
                    grid[r][c - 1] = 2;
                    count1--;
                    queue.add(new int[]{r, c - 1});
                }
                if (r + 1 < grid.length && grid[r + 1][c] == 1) {
                    grid[r + 1][c] = 2;
                    count1--;
                    queue.add(new int[]{r + 1, c});
                }
                if (c + 1 < grid[0].length && grid[r][c + 1] == 1) {
                    grid[r][c + 1] = 2;
                    count1--;
                    queue.add(new int[]{r, c + 1});
                }
            }
            System.out.println(count2);
            for (int i = 0; i < grid.length; i++) {
                for (int j = 0; j < grid[0].length; j++) {
                    System.out.print(grid[i][j]);
                }
                System.out.println();
            }
            System.out.println("====");
        }
        if (count1 > 0) {
            return -1;
        }else {
            return count2;
        }
    }
}