package com.whzcloud.leetCode9;

public class LeetCode997 {
    public static void main(String[] args) {
        int[][] trust = {{1,3},{2,3}};
        System.out.println(new Solution().findJudge(3, trust));
    }
}

class Solution {
    public int findJudge(int N, int[][] trust) {
        int[][] degree = new int[N][2];
        for (int[] item : trust) {
            degree[item[0] - 1][0]++;
            degree[item[1] - 1][1]++;
        }
        for (int i = 0; i < degree.length; i++){
            if (degree[i][0] == 0 && degree[i][1] == N - 1){
                return i + 1;
            }
        }
        return -1;
    }
}
