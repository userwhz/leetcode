package com.whzcloud.leetCode9;

import java.util.Arrays;

public class LeetCode977 {
    public static void main(String[] args) {
        int[] nums = {-7,-3,2,3,11};
        int[] res = new Solution977().sortedSquares(nums);
        for (int i : res) {
            System.out.println(i);
        }
    }
}


class Solution977 {
    public int[] sortedSquares(int[] nums) {
        int[] res = new int[nums.length];
        for (int i = 0, j = nums.length - 1, k = j; i <= j; ) {
            if (nums[i] * nums[i] <= nums[j] * nums[j]) {
                res[k] = nums[j] * nums[j];
                k--;
                j--;
            }else {
                res[k] = nums[i] * nums[i];
                k--;
                i++;
            }
        }
        return res;
    }
}