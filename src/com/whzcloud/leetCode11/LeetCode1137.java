package com.whzcloud.leetCode11;

public class LeetCode1137 {
    public static void main(String[] args) {

    }
}

class Solution {
    public int tribonacci(int n) {
        if (n == 0) return 0;
        if (n == 1) return 1;
        if (n == 2) return 1;
        int fir = 0;
        int sec = 1;
        int thi = 1;
        for (int i = 3; i <= n; i++){
            int sum = fir + sec + thi;
            fir = sec;
            sec = thi;
            thi = sum;
        }
        return thi;
    }
}
