package com.whzcloud.leetCode11;

public class LeetCode1143 {
    public static void main(String[] args) {
        System.out.println(new Solution1143().longestCommonSubsequence("bsbininm", "jmjkbkjkv"));
    }
}


class Solution1143 {
    public int longestCommonSubsequence(String text1, String text2) {
        int len1 = text1.length();
        int len2 = text2.length();
        int[][] dp = new int[len1 + 1][len2 + 1];
        for (int i = 1; i < len1 + 1; i++) {
            char char1 = text1.charAt(i - 1);
            for (int j = 1; j < len2 + 1; j++) {
                char char2 = text2.charAt(j - 1);
                if (char1 == char2) { // 开始列出状态转移方程
                    dp[i][j] = dp[i - 1][j - 1] + 1;
                } else {
                    dp[i][j] = Math.max(dp[i - 1][j], dp[i][j - 1]);
                }
            }
        }
        for (int i = 0; i < dp.length; i++) {
            for (int j = 0; j < dp[0].length; j++) {
                System.out.print(dp[i][j]);
            }
            System.out.println();
        }
        return dp[len1][len2];
    }
}
