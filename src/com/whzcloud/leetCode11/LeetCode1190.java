package com.whzcloud.leetCode11;

import java.util.Deque;
import java.util.LinkedList;
import java.util.Stack;

public class LeetCode1190 {
    public static void main(String[] args) {
        Solution1190 solution = new Solution1190();
        System.out.println(solution.reverseParentheses("(ed(et(oc))el)"));
    }
}

class Solution1190 {
    public String reverseParentheses(String s) {
        Deque<String> stack = new LinkedList<String>();
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < s.length(); i++) {
            char ch = s.charAt(i);
            if (ch == '(') {
                stack.push(sb.toString());
                sb.setLength(0);
            } else if (ch == ')') {
                sb.reverse();
                sb.insert(0, stack.pop());
            } else {
                sb.append(ch);
            }
        }
        return sb.toString();
    }
}

