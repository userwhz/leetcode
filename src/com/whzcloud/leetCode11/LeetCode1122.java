package com.whzcloud.leetCode11;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class LeetCode1122 {
    public static void main(String[] args) {
        int[] arr1 = {2,3,1,3,2,4,6,7,9,2,19};
        int[] arr2 = {2,1,4,3,9,6};
        int[] res = new int[arr1.length];
        res = new Solution1122().relativeSortArray(arr1, arr2);
        for (int i = 0; i < res.length; i++) {
            System.out.print(res[i]);
        }
    }
}


class Solution1122 {
    public int[] relativeSortArray(int[] arr1, int[] arr2) {
        int len1 = arr1.length;
        int len2 = arr2.length;
        int i = 0;
        int[] cnt = new int[1001];
        for (int n : arr1){
            cnt[n]++;
        }
        for (int n : arr2) {
            while (cnt[n] > 0) {
                arr1[i] = n;
                i++;
                cnt[n]--;
            }
        }
        for (int n = 0; n < cnt.length; n++) {
            while (cnt[n] > 0) {
                arr1[i] = n;
                i++;
                cnt[n]--;
            }
        }
        return arr1;

    }
}