package com.whzcloud.leetCode8;

public class LeetCode860 {
    public static void main(String[] args) {
        int[] bills = {5,5,5,10,20};
        System.out.println(new Solution860().lemonadeChange(bills));
    }
}

class Solution860 {
    public boolean lemonadeChange(int[] bills) {
        int five = 0;
        int ten = 0;
        for (int i = 0; i < bills.length; i++){
            if (bills[i] == 5){
                five++;
            }else if (bills[i] == 10){
                if (five == 0){
                    return false;
                }
                five--;
                ten++;
            }else {
                if (five > 0 && ten > 0){
                    five--;
                    ten--;
                }else if (five >= 3){
                    five -= 3;
                }else {
                    return false;
                }
            }
        }
        return true;
    }
}
