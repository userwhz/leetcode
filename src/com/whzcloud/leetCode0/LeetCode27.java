package com.whzcloud.leetCode0;

public class LeetCode27 {
    public static void main(String[] args) {
        int[] nums = {3, 2, 2, 3};
        int val = 2;
        Solution27 solution = new Solution27();
        System.out.println(solution.removeElement(nums, val));
    }
}

class Solution27 {
    public int removeElement(int[] nums, int val) {
        int i = 0;
        for (int j = 0; j < nums.length; j++) {
            if (nums[j] != val){
                nums[i] = nums[j];
                i++;
            }
        }
        return i ;
    }
}
