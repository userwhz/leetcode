package com.whzcloud.leetCode0;

public class LeetCode48 {
    public static void main(String[] args) {
        Solution48 solution = new Solution48();
        int[][] matrix = {{1,2,3}
                          ,{4,5,6}
                          ,{7,8,9}};
        solution.rotate(matrix);
        for (int i = 0; i < 3; i++){
            for (int j = 0; j < 3; j++){
                System.out.println(matrix[i][j]);
            }
        }
    }
}

class Solution48 {
    public void rotate(int[][] matrix) {
        int n = matrix.length;
        for (int i = 0; i < n / 2; ++i) {
            for (int j = 0; j < (n + 1) / 2; ++j) {
                int temp = matrix[i][j];
                matrix[i][j] = matrix[n - j - 1][i];
                matrix[n - j - 1][i] = matrix[n - i - 1][n - j - 1];
                matrix[n - i - 1][n - j - 1] = matrix[j][n - i - 1];
                matrix[j][n - i - 1] = temp;
            }
        }
    }
}
