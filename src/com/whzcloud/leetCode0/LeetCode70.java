package com.whzcloud.leetCode0;

public class LeetCode70 {
    public static void main(String[] args) {
        Solution70 solution = new Solution70();
        System.out.println(solution.climbStairs(45));
    }
}

class Solution70 {






    public int climbStairs(int n) {
        int pre = 1;
        int cur = 2;
        if (n == 1) {
            return 1;
        }
        if (n == 2) {
            return 2;
        }
        int res = 0;
        for (int i = 3; i <= n; i++) {
            res = pre + cur;
            pre = cur;
            cur = res;
        }
        return res;
    }

}
