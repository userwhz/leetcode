package com.whzcloud.leetCode0;

import java.util.HashMap;
import java.util.Map;

public class LeetCode3 {
    public static void main(String[] args) {
        String s = "abba";
        Solution3 solution = new Solution3();
        System.out.println(solution.lengthOfLongestSubstring(s));
    }
}
class Solution3 {
    public int lengthOfLongestSubstring(String s) {
        int start = 0;
        int ans = 0;
        HashMap<Character, Integer> map = new HashMap<>();
        for (int end = 0; end < s.length(); end++) {
            char c = s.charAt(end);
            if (map.containsKey(c)) {
                start = Math.max(start, map.get(c));
            }
            ans = Math.max(ans, end - start + 1);
            map.put(c, end + 1);
        }
        return ans;
    }
}
