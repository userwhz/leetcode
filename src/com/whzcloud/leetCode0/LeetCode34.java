package com.whzcloud.leetCode0;

public class LeetCode34 {
    public static void main(String[] args) {
        Solution34 solution = new Solution34();
        int[] nums = {5,7,7,8,8,10};
        int target = 6;
        int[] res = solution.searchRange(nums, target);
        for (int i = 0; i < res.length; i++){
            System.out.println(res[i]);
        }

    }
}

class Solution34 {
    public int[] searchRange(int[] nums, int target) {
        int[] ans = {-1, -1};
        int n = nums.length;
        if (n == 0) {
            return ans;
        }
        int l = 0;
        int r = n - 1;
        while (l < r) {
            int mid = (l + r) / 2;
            if (nums[mid] >= target) {
                r = mid;
            }else {
                l = mid + 1;
            }
        }
        if (nums[l] != target) {
            return ans;
        }else {
            ans[0] = l;
            l = 0;
            r = n - 1;
            while (l < r) {
                int mid = (l + r + 1) / 2;
                if (nums[mid] <= target) {
                    l = mid;
                }else {
                    r = mid - 1;
                }
            }
            ans[1] = l;
            return ans;
        }
    }
}
