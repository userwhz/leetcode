package com.whzcloud.leetCode0;

public class LeetCode7 {
    public static void main(String[] args) {
        Solution7 solution = new Solution7();
        System.out.println(solution.reverse(123));
    }
}

class Solution7 {
    public int reverse(int x) {
        long res = 0;
        while(x != 0) {
            res = res * 10 + x % 10;
            x = x/10;
        }
        return (int)res == res? (int)res:0;
    }
}