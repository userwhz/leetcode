package com.whzcloud.leetCode0;

public class LeetCode11 {
    public static void main(String[] args) {
        int[] height = {1,8,6,2,5,4,8,3,7};
        Solution11 solution = new Solution11();
        System.out.println(solution.maxArea(height));
    }
}
class Solution11 {
    public int maxArea(int[] height) {
        int i = 0;
        int j = height.length - 1;
        int res = 0;
        while (i < j) {
            if (height[i] < height[j]) {
                res = Math.max(res, height[i] * (j - i));
                i++;
            }else {
                res = Math.max(res, height[j] * (j - i));
                j--;
            }
        }
        return res;
    }
}