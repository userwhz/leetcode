package com.whzcloud.leetCode0;

public class LeetCode98 {
    public static void main(String[] args) {

    }
}
class Solution98 {
    //BST 中序递增
    long pre = Long.MIN_VALUE;
    public boolean isValidBST(TreeNode root) {
        if (root == null) {
            return true;
        }

        if (!isValidBST(root.left)){
            return false;
        }
        if (root.val <= pre){
            return false;
        }
        pre = root.val;
        if (!isValidBST(root.right)){
            return false;
        }
        return true;
    }
}