package com.whzcloud.leetCode0;

import com.sun.javafx.collections.ChangeHelper;

import java.util.Stack;

public class LeetCode24 {
    public static void main(String[] args) {
        Solution24 solution = new Solution24();
        ListNode l1 = new ListNode(4);
        ListNode l2 = new ListNode(3, l1);
        ListNode l3 = new ListNode(2, l2);
        ListNode l4 = new ListNode(1, l3);
        ListNode listNode = solution.swapPairs(l4);
        while (listNode != null) {
            System.out.println(listNode.val);
            listNode = listNode.next;
        }
    }
}

//时间复杂度：O(n)
class Solution24 {
    public ListNode swapPairs(ListNode head) {
        if (head == null || head.next == null) {
            return head;
        }
        ListNode pre = head;
        ListNode cur = pre.next;
        int temp = cur.val;
        cur.val = pre.val;
        pre.val = temp;
        while (cur.next != null && cur.next.next != null) {
            pre = pre.next.next;
            cur = cur.next.next;
            int tmp = cur.val;
            cur.val = pre.val;
            pre.val = tmp;
        }
        return head;
    }
}
