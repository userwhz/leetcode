package com.whzcloud.leetCode0;

public class LeetCode55 {
    public static void main(String[] args) {
        int[] nums=  {2,5,1,1,0, 1};
        System.out.println(new Solution55().canJump(nums));
    }
}

class Solution55 {
    public boolean canJump(int[] nums) {
        int k = 0;
        for (int i = 0; i < nums.length; i++) {
            if (i > k) return false;
            k = Math.max(k, i + nums[i]);
            if (k >= nums.length){
                return true;
            }
        }
        return true;
    }
}