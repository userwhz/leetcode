package com.whzcloud.leetCode0;

import java.util.HashMap;
import java.util.Map;

public class LeetCode41 {
    public static void main(String[] args) {
        System.out.println(new Solution41().firstMissingPositive(new int[]{0,1,1,2,2}));
    }
}

class Solution41 {
    public int firstMissingPositive(int[] nums) {
        Map<Integer, Integer> map = new HashMap<>();
        int j = 1;
        for (int i = 0; i < nums.length; i++) {
            if (nums[i] <= 0) {
                continue;
            }else {
                map.put(nums[i], map.getOrDefault(nums[i], 0) + 1);
                while (map.getOrDefault(j, 0) != 0) {
                    j++;
                }
            }
        }
        return j;
    }
}
