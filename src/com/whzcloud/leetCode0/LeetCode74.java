package com.whzcloud.leetCode0;

public class LeetCode74 {
    public static void main(String[] args) {
        int[][] matrix = {{1,3,5,7},
                {10,11,16,20},
                {23,30,34,60}};
        System.out.println(new Solution74().searchMatrix(matrix, 3));
    }
}
class Solution74 {
    public boolean searchMatrix(int[][] matrix, int target) {
        int rows = matrix.length - 1, columns = 0;
        while (rows >= 0 && columns < matrix[0].length) {
            int num = matrix[rows][columns];
            if (num == target) {
                return true;
            } else if (num > target) {
                rows--;
            } else {
                columns++;
            }
        }
        return false;
    }


}