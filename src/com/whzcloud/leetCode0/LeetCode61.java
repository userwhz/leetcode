package com.whzcloud.leetCode0;

import java.util.List;

public class LeetCode61 {

}

class Solution61 {
    public ListNode rotateRight(ListNode head, int k) {
        if (head == null ||k == 0) {
            return head;
        }
        int len = 0;
        ListNode tail = null;
        ListNode cur = head;
        for(cur = head; cur != null ; cur = cur.next){
            tail = cur;
            len++;
        }
        k = k % len;
        cur = head;
        for (int i = 0; i < len - k - 1; i++) {
            cur = cur.next;
        }
        tail.next = head;
        head = cur.next;
        cur.next = null;
        return head;
    }
}