package com.whzcloud.leetCode0;

public class LeetCode59 {
    public static void main(String[] args) {

    }
}

class Solution59 {
    public int[][] generateMatrix(int n) {
        int[][] res = new int[n][n];
        int up = 0;
        int down = n - 1;
        int left = 0;
        int right = n - 1;
        int cnt = 1;
        int tar = n * n;
        while (cnt <= tar) {
            for (int i = left; i <= right; i++) {
                res[up][i] = cnt;
                cnt++;
            }
            up++;
            for (int i = up; i <= down; i++) {
                res[i][right] = cnt;
                cnt++;
            }
            right--;
            for (int i = right; i >= left; i--) {
                res[down][i] = cnt;
                cnt++;
            }
            down--;
            for (int i = down; i >= up; i--) {
                res[i][left] = cnt;
                cnt++;
            }
            left++;
        }
        return res;
    }
}

