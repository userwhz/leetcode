package com.whzcloud.leetCode0;

public class LeetCode35 {
    public static void main(String[] args) {
        Solution35 solution = new Solution35();
        int[] nums = {1,3,5,6};
        int target = 2;
        System.out.println(solution.searchInsert(nums, target));
    }
}

class Solution35 {
    public int searchInsert(int[] nums, int target) {
        int left = 0;
        int right = nums.length - 1;
        while (left <= right) {
            int mid = left + (right - left) / 2;
            if (nums[mid] == target) {
                return mid;
            }else if (nums[mid] < target) {
                left = mid + 1;
            }else {
                right = mid - 1;
            }
        }
        return left;
    }
}