package com.whzcloud.leetCode0;

import java.util.*;

public class LeetCode17 {
    public static void main(String[] args) {
        Solution17 solution = new Solution17();
        System.out.println(solution.letterCombinations("23"));
    }
}

class Solution17 {
    public List<String> letterCombinations(String digits) {
        if (digits == null || digits.length() == 0) {
            return new ArrayList<>();
        }
        Map<Character, String> map = new HashMap<>();
        map.put('2', "abc");
        map.put('3', "def");
        map.put('4', "ghi");
        map.put('5', "jkl");
        map.put('6', "mno");
        map.put('7', "pqrs");
        map.put('8', "tuv");
        map.put('9', "wxyz");
        List<String> res = new LinkedList<>();
        search("", digits, 0, res, map);
        return res;
    }


    private void search(String s, String dights, int i, List<String> res,
                        Map<Character, String> map){
        if (i == dights.length()) {
            res.add(s);
            return;
        }
        String letters = map.get(dights.charAt(i));
        for (int j = 0; j < letters.length(); j++) {
            search(s + letters.charAt(j), dights, i + 1, res, map);
        }

    }
}