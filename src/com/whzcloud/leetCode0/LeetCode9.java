package com.whzcloud.leetCode0;

public class LeetCode9 {
    public static void main(String[] args) {
        Solution9 solution = new Solution9();
        System.out.println(solution.isPalindrome(121));
    }
}

class Solution9 {
    public boolean isPalindrome(int x) {
        if (x < 0){
            return false;
        }
        int res = 0;
        int xx = x;
        while (x != 0) {
            res = res * 10 + x % 10;
            x /= 10;
        }
        if (res == xx){
            return true;
        }else {
            return false;
        }
    }
}