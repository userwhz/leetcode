package com.whzcloud.leetCode0;

import java.util.ArrayList;
import java.util.List;

public class LeetCode78 {
    public static void main(String[] args) {

    }
}

class Solution {
    public List<List<Integer>> subsets(int[] nums) {
        List<List<Integer>> res = new ArrayList<>();
        dfs(res, nums, new ArrayList<>(), 0);
        return res;
    }

    private void dfs(List<List<Integer>> res, int[] nums, ArrayList<Integer> list, int index) {
        if (index == nums.length) {
            res.add(new ArrayList<>(list));
            return;
        }

        dfs(res, nums, list, index + 1);

        list.add(nums[index]);
        dfs(res, nums, list, index + 1);

        list.remove(list.size() - 1);

    }
}