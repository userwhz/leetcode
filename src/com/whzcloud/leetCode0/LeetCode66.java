package com.whzcloud.leetCode0;

public class LeetCode66 {
    public static void main(String[] args) {
        Solution66 solution = new Solution66();
        int[] digits = {9};
        int[] res = solution.plusOne(digits);
        for (int i = 0; i < res.length; i++){
            System.out.println(res[i]);
        }
    }
}

class Solution66 {
    public int[] plusOne(int[] digits) {
        int flag = 0;

        for (int i = digits.length - 1; i >= 0; i--) {
            int sum = 0;
            if (i == digits.length - 1) {
                sum = digits[i] + flag + 1;
            } else {
                sum = digits[i] + flag;
            }
            if (i == 0 && sum >= 10) {
                digits[0] = sum - 10;
                int[] res = new int[digits.length + 1];
                res[0] = 1;
                for (int j = 1; j < res.length; j++) {
                    res[j] = digits[j - 1];
                }
                return res;
            }
            flag = 0;
            if (sum < 10) {
                digits[i] += 1;
                break;
            }else if (sum == 10){
                digits[i] = 0;
                flag = 1;
            }else if (sum > 10) {
                digits[i] = sum - 10;
                flag = 1;
            }
        }
        return digits;
    }
}
