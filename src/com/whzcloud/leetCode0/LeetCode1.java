package com.whzcloud.leetCode0;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class LeetCode1 {
    public static void main(String[] args) {
        int[] nums = new int[]{1,2,3,4,5};
        Solution1 s = new Solution1();
        System.out.println(Arrays.toString(s.twoSum(nums,4)));
    }
}

class Solution1 {
    public int[] twoSum(int[] nums, int target) {
        Map<Integer, Integer> map = new HashMap<>();
        for (int i = 0; i < nums.length; i++) {
            if (map.containsKey(nums[i])){
                return new int[]{map.get(nums[i]), i};
            }
            int res = target - nums[i];
            map.put(res, i);
        }
        return null;
    }
}

//class Solution1 {
//    public int[] twoSum(int[] nums, int target) {
//        for (int i = 0; i < nums.length; i++) {
//            for (int j = i + 1; j < nums.length; j++){
//                if (nums[i] + nums[j] == target){
//                    return new int[]{i,j};
//                }
//            }
//        }
//        return null;
//    }
//}