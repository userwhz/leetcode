package com.whzcloud.leetCode0;

import java.util.ArrayList;
import java.util.List;

public class LeetCode22 {
    public static void main(String[] args) {
        Solution22 solution = new Solution22();
        System.out.println(solution.generateParenthesis(3));
    }
}

class Solution22 {
    List<String> list = new ArrayList<>();
    public List<String> generateParenthesis(int n) {
        generate(0, 0, n, "");
        return list;
    }

    public void generate(int left, int right, int n, String s) {
        //terminator
        if (left == n && right == n) {
            list.add(s);
            return;
        }
        //process current logic : left ,right

        //drill down
        if (left < n) {
            generate(left + 1, right, n, s + "(");
        }
        if (right < left && right < n) {
            generate(left, right + 1, n, s + ")");
        }
        //reverse states
    }
}

