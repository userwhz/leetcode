package com.whzcloud.leetCode0;

import java.util.ArrayList;
import java.util.Collections;

public class LeetCode23 {
    public static void main(String[] args) {
        Solution23 solution = new Solution23();
        ListNode l1 = new ListNode(5);
        ListNode l2 = new ListNode(4, l1);
        ListNode l3 = new ListNode(1, l2);
        ListNode l4 = new ListNode(4);
        ListNode l5 = new ListNode(3, l4);
        ListNode l6 = new ListNode(1, l5);
        ListNode l7 = new ListNode(6);
        ListNode l8 = new ListNode(2, l7);
        ListNode[] lists = {l3, l5, l8};
        ListNode listNode = solution.mergeKLists(lists);
        while (listNode != null) {
            System.out.println(listNode.val);
            listNode = listNode.next;
        }
    }
}

class Solution23 {
    public ListNode mergeKLists(ListNode[] lists) {
        ArrayList<Integer> list = new ArrayList<>();
        for (int i = 0; i < lists.length; i++){
            while (lists[i] != null){
                list.add(lists[i].val);
                lists[i] = lists[i].next;
            }
        }
        Collections.sort(list);
        ListNode listNode = new ListNode();
        ListNode cur = listNode;
        for (int i = 0; i < list.size(); i++){
            ListNode listNode1 = new ListNode(list.get(i));
            listNode.next = listNode1;
            listNode = listNode.next;
        }
        return cur.next;
    }
}
