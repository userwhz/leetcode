package com.whzcloud.leetCode0;

import java.util.Stack;

public class LeetCode20 {
    public static void main(String[] args) {
        Solution20 solution = new Solution20();
        System.out.println(solution.isValid("()"));
    }
}

class Solution20 {
    public boolean isValid(String s) {
        Stack<Character> stack = new Stack<>();
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            if (c == '(') {
                stack.push(')');
            }else if (c == '{') {
                stack.push('}');
            }else if (c == '[') {
                stack.push(']');
            }else if (stack.isEmpty() || c != stack.pop()) {
                return false;
            }
        }
        if (stack.isEmpty()) {
            return true;
        }else {
            return false;
        }
    }
}