package com.whzcloud.leetCode0;

public class LeetCode44 {
    public static void main(String[] args) {

        System.out.println(new Solution44().isMatch("adscasb","a*b"));
    }
}
class Solution44 {

    public boolean isMatch(String s, String p) {
        int m = p.length();
        int n = s.length();
        boolean[][] dp = new boolean[m + 1][n + 1];
//        for (int i = 0; i <= m; i++) {
//            for (int j = 0; j <= n; j++) {
//                System.out.print(dp[i][j]);
//            }
//            System.out.println();
//        }
//        System.out.println("=====");
        dp[0][0] = true;
        for (int i = 1; i < m + 1; i++) {
            if (p.charAt(i - 1) == '*') {
                dp[i][0] = true;
            }else {
                break;
            }
        }
        for (int i = 1; i <= m; i++) {
            for (int j = 1; j <= n; j++) {
                if (p.charAt(i - 1) == s.charAt(j - 1) || p.charAt(i - 1) == '?') {
                    dp[i][j] = dp[i - 1][j - 1];
                }else if (p.charAt(i - 1) == '*' ) {
                    if (dp[i - 1][ j] == true || dp[i][j - 1] == true){
                        dp[i][j] = true;
                        for (int k = j; k <= n; k++) {
                            dp[i][k] = true;
                        }
                    }
                }
            }
        }
//        for (int i = 0; i <= m; i++) {
//            for (int j = 0; j <= n; j++) {
//                System.out.print(dp[i][j] + " ");
//            }
//            System.out.println();
//        }
        return dp[m][n];
    }

}
