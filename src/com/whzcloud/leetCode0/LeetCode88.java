package com.whzcloud.leetCode0;

import java.util.Arrays;

public class LeetCode88 {
    public static void main(String[] args) {
        int[] num1 = {1,2,3,0,0,0};
        int[] num2 = {2,5,6};
        new Solution88().merge(num1, 3, num2, 3);
        for (int i = 0; i < num1.length; i++) {
            System.out.println(num1[i]);
        }
    }
}

class Solution88 {
    public void merge(int[] nums1, int m, int[] nums2, int n) {
        int[] res = new int[m + n];
        int p = 0;
        int q = 0;
        int cur;
        while (p < m || q < n) {
            if (p == m) {
                cur = nums2[q++];
            }else if (q == n) {
                cur = nums1[p++];
            }else if (nums1[p] < nums2[q]){
                cur = nums1[p++];
            }else {
                cur = nums2[q++];
            }
            res[p + q - 1] = cur;
        }
        for (int i = 0; i < m + n; i++) {
            nums1[i] = res[i];
        }
    }
}
