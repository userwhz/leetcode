package com.whzcloud.leetCode0;

import java.util.ArrayList;
import java.util.List;

public class LeetCode94 {
    public static void main(String[] args) {
        Solution94 solution = new Solution94();
        TreeNode treeNode3 = new TreeNode(3);
        TreeNode treeNode2 = new TreeNode(2, treeNode3, null);
        TreeNode treeNode1 = new TreeNode(1, null, treeNode2);
        System.out.println(solution.inorderTraversal(treeNode1));
    }
}

class Solution94 {

    public List<Integer> inorderTraversal(TreeNode root) {
        List<Integer> list = new ArrayList<>();
        if (root == null) {
            return list;
        }
        inTraversal(root, list);
        return list;
    }
    public void inTraversal(TreeNode root, List<Integer> list){
        if (root == null) {
            return;
        }
        inTraversal(root.left, list);
        list.add(root.val);
        inTraversal(root.right, list);
    }

}

class TreeNode {
    int val;
    TreeNode left;
    TreeNode right;

    TreeNode() {}

    TreeNode(int val) {
        this.val = val;
    }

    TreeNode(int val, TreeNode left, TreeNode right) {
        this.val = val;
        this.left = left;
        this.right = right;
    }
}
