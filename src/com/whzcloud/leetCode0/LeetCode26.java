package com.whzcloud.leetCode0;

public class LeetCode26 {
    public static void main(String[] args) {
        Solution26 solution = new Solution26();
        int[] nums = {0, 0, 1, 1, 1, 2, 2, 3, 4};
        System.out.println(solution.removeDuplicates(nums));
    }
}
class Solution26 {
    public int removeDuplicates(int[] nums) {
        int k = 1;
        for (int i = 1; i < nums.length; i++) {
            if (nums[i] == nums[i - 1]) {
                continue;
            }else {
                nums[k] = nums[i];
                k++;
            }
        }
        return k;
    }
}
