package com.whzcloud.leetCode0;


import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class LeetCode95 {
    public static void main(String[] args) {
        Solution95 solution = new Solution95();
        List<TreeNode> treeNode = solution.generateTrees(3);

    }
}

class Solution95 {
    public List<TreeNode> generateTrees(int n) {
        if (n == 0) {
            return new LinkedList<TreeNode>();
        }
        return generateTrees(1, n);
    }

    public List<TreeNode> generateTrees(int start, int end) {
        List<TreeNode> allTrees = new LinkedList<TreeNode>();
        if (start > end){
            allTrees.add(null);
            return allTrees;
        }

        //枚举可行根节点
        for (int i = start; i <= end; i++) {
            //获取所有可行的左子树集合
            List<TreeNode> leftTrees = generateTrees(start, i - 1);
            //获取所有可行的右子树集合
            List<TreeNode> rightTrees = generateTrees(i + 1, end);
            //从左子树集合中选出一颗左子树，从右子树集合中选出一颗右子树，拼接到根节点上
            for(TreeNode left : leftTrees){
                for (TreeNode right : rightTrees){
                    TreeNode currrTree = new TreeNode(i);
                    currrTree.left = left;
                    currrTree.right = right;
                    allTrees.add(currrTree);
                }
            }
        }
        return allTrees;
    }

}