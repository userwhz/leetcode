package com.whzcloud.leetCode0;

import java.util.Arrays;
import java.util.Stack;

public class LeetCode84 {
    public static void main(String[] args) {
        Solution84 solution = new Solution84();
        int[] height = {2,1,5,6,2,3};
        System.out.println(solution.largestRectangleArea(height));
    }
}

class Solution84 {
    public int largestRectangleArea(int[] heights) {
        int n = heights.length;
        int[] left = new int[n];
        int[] right = new int[n];
        Arrays.fill(right, n);
        Stack<Integer> stack = new Stack<>();
        for (int i = 0; i < n; i++) {
            while (!stack.isEmpty() && heights[stack.peek()] >= heights[i]) {
                right[stack.peek()] = i;
                stack.pop();
            }
            left[i] = (stack.isEmpty() ? -1 : stack.peek());
            stack.push(i);
        }
        int ans = 0;
        for (int i = 0; i < n; i++) {
            ans = Math.max(ans, (right[i] - left[i] - 1) * heights[i]);
        }
        return ans;
    }
}