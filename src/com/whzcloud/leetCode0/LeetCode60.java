package com.whzcloud.leetCode0;

import java.util.HashSet;
import java.util.Set;

public class LeetCode60 {
    public static void main(String[] args) {
        System.out.println(new Solution60().getPermutation(3, 3));
    }
}

class Solution60 {
    String res = "";
    int cnt = 0;
    public String getPermutation(int n, int k) {
        int[] visited = new int[n + 1];
        backTrack(visited, n, k, "");
        return res;
    }

    private boolean backTrack(int[] visited, int n, int k, String cur) {
        if (cur.length() == n){
            cnt++;
        }
        if (cnt == k && cur.length() == n){
            res = cur;
            return true;
        }

        for (int i = 1; i <= n; i++) {
            if (visited[i] == 0) {
                visited[i] = 1;
                if (backTrack(visited, n, k, cur + i)){
                    return true;
                }
                visited[i] = 0;
            }
        }
        return false;
    }

}