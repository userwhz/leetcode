package com.whzcloud.leetCode0;

public class LeetCode50 {
    public static void main(String[] args) {
        System.out.println(new Solution50().myPow(2,5));
    }
}
//暴力破解  累乘  o(n)
//分治    o(log n)
//牛顿迭代法
class Solution50 {
    public double myPow(double x, int n) {
        long N = n;
        return N > 0 ? quickMul(x, N) : 1.0 / quickMul(x, -N);
    }

    private double quickMul(double x, long N) {
        if (N == 0) {
            return 1.0;
        }
        double y = quickMul(x, N / 2);
        return N % 2 == 0 ? y * y : y * y * x;
    }


}