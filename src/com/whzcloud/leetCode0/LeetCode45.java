package com.whzcloud.leetCode0;

public class LeetCode45 {
    public static void main(String[] args) {

    }
}

class Solution45 {
    public int jump(int[] nums) {
        int len = nums.length - 1;
        int res = 0;
        while (len > 0) {
            for (int i = 0; i < len; i++) {
                if (i + nums[i] >= len) {
                    len = i;
                    res++;
                    break;
                }
            }
        }
        return res;
    }

}
