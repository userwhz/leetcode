package com.whzcloud.leetCode0;

import java.util.ArrayList;
import java.util.List;

public class LeetCode54 {
    public static void main(String[] args) {
        int[][] matrix = {{1,2,3,4},{5,6,7,8},{9,10,11,12},{13,14,15,16}};
        List<Integer> list = new Solution54().spiralOrder(matrix);
        for (int i : list) {
            System.out.print(i + " ");
        }
    }
}

class Solution54 {
    int[] dx = {0,1,0,-1};
    int[] dy = {1,0,-1,0};
    int dict = 0;
    public List<Integer> spiralOrder(int[][] matrix) {
        boolean[][] ready = new boolean[matrix.length][matrix[0].length];
        int max = matrix.length * matrix[0].length;
        List<Integer> res= new ArrayList<>();
        dfs(dict, res, matrix, ready, 0, 0, max);
        return res;
    }

    private void dfs(int dict, List<Integer> list, int[][] matrix, boolean[][] ready, int i, int j, int max) {
        list.add(matrix[i][j]);
        for (int m = 0; m < matrix.length; m++) {
            for (int n = 0; n < matrix[0].length; n++) {
                System.out.print(ready[m][n] + " ");
            }
            System.out.println();
        }
        ready[i][j] = true;
        if (list.size() == max) {
            return;
        }

        for (int k = 0; k < 4; k++) {
            int ddx = i + dx[(dict + 4) % 4];
            int ddy = j + dy[(dict + 4) % 4];
            if (ddx >= 0 && ddx < matrix.length && ddy >= 0 && ddy < matrix[0].length && ready[ddx][ddy] != true) {
                System.out.println(ddx + " " + ddy);
                dfs(dict,list, matrix, ready, ddx, ddy, max);
            }else {
                dict++;
            }
        }
    }
}