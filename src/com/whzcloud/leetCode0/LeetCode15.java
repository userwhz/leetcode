package com.whzcloud.leetCode0;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class LeetCode15 {
    public static void main(String[] args) {
        int[] nums = {-1,0,1,2,-1,-4};
        Solution15 solution = new Solution15();
        System.out.println(solution.threeSum(nums));
    }
}
//-3 -2 -2 0 2 4
//三重循环
//哈希表
//双指针
class Solution15 {
    public List<List<Integer>> threeSum(int[] nums) {
        Arrays.sort(nums);
        List<List<Integer>> lists = new ArrayList<>();
        for (int k = 0; k < nums.length - 2; k++) {
            if (nums[k] > 0)
                break;
            if (k > 0 && nums[k] == nums[k - 1])
                continue;
            int i = k + 1;
            int j = nums.length - 1;
            while (i < j) {
                int sum = nums[i] + nums[j] + nums[k];
                if (sum < 0) {
                    i++;
                    while (i < j && nums[i] == nums[i - 1]) {
                        i++;
                    }
                }else if (sum > 0) {
                    j--;
                    while (i < j && nums[j] == nums[j + 1]) {
                        j--;
                    }
                }else {
                    lists.add(Arrays.asList(nums[k], nums[i], nums[j]));
                    i++;
                    while (i < j && nums[i] == nums[i - 1]) {
                        i++;
                    }
                    j--;
                    while (i < j && nums[j] == nums[j + 1]) {
                        j--;
                    }
                }
            }
        }
        return lists;
    }
}