package com.whzcloud.leetCode0;

public class LeetCode96 {
    public static void main(String[] args) {
        Solution96 solution = new Solution96();
        System.out.println(solution.numTrees(3));
    }
}

class Solution96 {
    public int numTrees(int n) {
        int[] dp = new int[n+1];
        dp[0] = 1;
        dp[1] = 1;
        for(int i = 2; i < n + 1; i++) {
            for(int j = 1; j < i + 1; j++) {
                dp[i] += dp[j-1] * dp[i-j];
            }
        }
        return dp[n];
    }
}
