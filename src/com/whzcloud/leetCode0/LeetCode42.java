package com.whzcloud.leetCode0;

public class LeetCode42 {
    public static void main(String[] args) {
        Solution42 solution = new Solution42();
        int[] height = {0,1,0,2,1,0,1,3,2,1,2,1};
        System.out.println(solution.trap(height));
    }
}
class Solution42 {
    public int trap(int[] height) {
        int ans = 0;
        int len = height.length;
        for (int i = 1; i < len; i++) {
            int max_left = 0;
            int max_right = 0;
            for (int j = 0; j <= i; j++) {
                max_left = Math.max(max_left, height[j]);
            }
            for (int j = i; j < len; j++) {
                max_right = Math.max(max_right, height[j]);
            }
            ans += Math.min(max_left, max_right) - height[i];
        }
        return ans;

    }
}