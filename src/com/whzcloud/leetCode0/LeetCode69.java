package com.whzcloud.leetCode0;

public class LeetCode69 {
    public static void main(String[] args) {

        System.out.println(new Solution69().mySqrt(2147395599
        ));
    }
}

class Solution69 {
    public int mySqrt(int x) {
        if (x == 0 || x == 1) {
            return x;
        }
        int left = 0;
        int right = x;
        int mid = 0;
        while (left <= right) {
            mid = left + (right - left) / 2;
            if ((long) mid * mid == x) {
                return mid;
            }else if ((long) mid * mid > x){
                right = mid - 1;
            }else {
                left = mid + 1;
            }
        }
        if ((long) mid * mid < x) {
            return mid;
        }else {
            return mid - 1;
        }
    }
}
