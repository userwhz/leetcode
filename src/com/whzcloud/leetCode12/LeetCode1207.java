package com.whzcloud.leetCode12;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class LeetCode1207 {
    public static void main(String[] args) {
        int[] arr = {1,2,2,1,1,3};
        System.out.println(new Solution1207().uniqueOccurrences(arr));
    }
}

class Solution1207 {
    public boolean uniqueOccurrences(int[] arr) {
        Map<Integer, Integer> map = new HashMap<>();
        for (int x : arr){
            map.put(x, map.getOrDefault(x, 0) + 1);
        }
        Set<Integer> set = new HashSet<>();
        for (int m : map.keySet()){
            set.add(map.get(m));
        }
        return map.size() == set.size();
    }
}
